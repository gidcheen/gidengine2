#pragma once

#include <Graphics/Model.hpp>
#include <Game.hpp>
#include <Assets/Asset.hpp>
#include <Assets/Loaders/LoaderModel.hpp>
#include <Graphics/Material.hpp>
#include <GL/GLTexture.hpp>
#include <Utils/Callback.hpp>

#include <GL/GLProgram.hpp>
#include <GL/GLProgramPipeline.hpp>

using namespace GidEngine;
using namespace Gid;
using namespace Assets;
using namespace GL;

class TestGame : public Game
{
private:
	double t = 0;

	shared_ptr<Asset<Model>> model;
	shared_ptr<GLProgram> vert;
	shared_ptr<GLProgram> frag;
	shared_ptr<GLProgramPipeline> shader;

public:
	TestGame();
	~TestGame() override = default;

protected:
	void Update() override;
};
