#include "TestGame.hpp"

#include <GL/GLProgram.hpp>
#include <GL/GLProgramPipeline.hpp>
#include <GL/GLBuffer.hpp>
#include <GL/GLVertexArray.hpp>
#include <GL/GLProgram.hpp>
#include <GL/GLProgramPipeline.hpp>
#include <GL/GLFrameBuffer.hpp>
#include <Graphics/Mesh.hpp>
#include <Scene/Vertex.hpp>
#include <ECS/Manager.hpp>
#include <ECS/Entity.hpp>
#include <Graphics/RenderSteps/GLObjectBindStep.hpp>
#include <Graphics/RenderSteps/GLDrawElementsStep.hpp>
#include <Gid/Math/Quat.hpp>
#include <Serialization/PolymorphicSerializer.hpp>
#include <Serialization/PolymorphicSerializer.hpp>
#include <Window.hpp>
#include <memory>
#include <Scene/Components/TransformComponent.hpp>
#include <Assets/AssetManager.hpp>
#include <Assets/Loaders/LoaderGLTexture.hpp>

#include <Utils/PerThreadEventQueues.hpp>

#include <Serialization/JsonSerializer.hpp>
//#include <Serialization/Serializers/SerializerMat.hpp>
#include <Serialization/SerializationDefinition.hpp>
//#include <Serialization/Serializers/SerializerBasic.hpp>
#include <Serialization/JsonSerializer.hpp>

struct Ser
{
	int a = 0;
	float b = 0;

	float GetB() const { return b; }
	void SetB(float b) { this->b = b; }
};

template<typename TSerializer>
struct SerializationDefinition<TSerializer, Ser>
{
	static vector<FieldSerializer<TSerializer, Ser>> & GetFieldSerializers()
	{
		static vector<FieldSerializer<TSerializer, Ser>> fieldSerializers;
		if (fieldSerializers.empty())
		{
			fieldSerializers.push_back(FieldSerializer<TSerializer, Ser>(Name("a"), &Ser::a));
			fieldSerializers.emplace_back(Name("b"), &Ser::GetB, &Ser::SetB);
		}
		return fieldSerializers;
	}
};

TestGame::TestGame()
{
    auto mata = Mat4f::Identity();
    auto matb = Mat4f::Identity();
    matb[1][0] = 1;
    matb[1][1] = 1;
    matb[1][2] = 1;

    auto matc = mata * matb;

	auto const[a, b] = Vec2f(1, 2).elements;

	auto testt0 = TypeName(a);

	int ci = 0;
	int const * const cir = &ci;
	auto testt1 = TypeName(ci);
	auto testt2 = TypeName(&cir);



	struct Serser2 : public Ser
	{
		int yoyoo = 0;
	};
	Serser2 serser2;

	Ser * serser2p = &serser2;

	auto ss2pn = Demangle(typeid(serser2p).name());

	auto const & w = window;

	auto testt3 = TypeName(w);

	Ser ser1{ 10, 10 };
	Ser ser2{ 0, 0 };

	JsonV serJson1;
	JsonSerializer jsonSerializer;
	jsonSerializer.Serialize(serJson1, ser1);

	JsonV serJson2;
	jsonSerializer.Serialize(serJson2, ser1);
	jsonSerializer.Deserialize(serJson2, ser2);

	vert = make_shared<GLProgram>(
		GL_VERTEX_SHADER,
		vector<string>{
			"#version 450\n"
			"layout (location = 0) in vec3 position;\n"
			"layout (location = 1) in vec3 normal;\n"
			"//layout (location = 2) in vec3 tangent;\n"
			"//layout (location = 3) in vec3 bitangent;\n"
			"//layout (location = 4) in vec3 color;\n"
			"//layout (location = 5) in vec2 uv;\n"
			"out vec3 onormal;\n"
			"uniform mat4 projectionMat = mat4(1.0);\n"
			"out gl_PerVertex{ vec4 gl_Position; };\n"
			"void main(){ onormal = normal; gl_Position = projectionMat *  vec4(position, 1); }" });

	frag = make_shared<GLProgram>(
		GL_FRAGMENT_SHADER,
		vector<string>{
			"#version 450\n"
			"in vec3 onormal;\n"
			"out vec4 outColor;\n"
			"void main(){ outColor = vec4(onormal, 1); }" });

	shader = make_shared<GLProgramPipeline>();
	shader->Add(frag.get());
	shader->Add(vert.get());
	//shader->SetUniform("projectionMat", Math::Orthographic<float>(-5, 5, -5, 5, -20, 20));
	shader->SetUniform("projectionMat", Math::Perspective<float>(70, 16.f / 9.f, 1, 100));

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	model = AssetManager::GetInstance()->GetAsset<Model>("Models/Sphere.dae");


	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
}

void TestGame::Update()
{
	PerThreadEventQueues::Get().ExecuteAll();

	t += GetDt();
	if (t > 0.2f)
	{
		window.SetTitle(to_string(1 / GetDt()) + " FPS");
		t = 0;
	}

	if (!model->asset) { return; }

	for (auto const & m : model->asset->GetMeshes())
	{
		Renderable r;

		r.steps.push_back(make_shared<GLObjectBindStep<GLProgramPipeline>>(shader));
		r.steps.push_back(make_shared<GLObjectBindStep<GLVertexArray>>(m->GetVertexArray()));
		r.steps.push_back(make_shared<GLObjectBindStep<GLIndexBuffer >>(m->GetIndexBuffer()));
		r.steps.push_back(make_shared<GLDrawElementsStep>(m->GetIndexBuffer()->GetCount()));

		renderer.Submit(r);
	}
}
