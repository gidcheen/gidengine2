#include "TestGame.hpp"

#include <MainLoop.hpp>
#include <memory>

using namespace std;
using namespace GidEngine;
using namespace Gid;

int main()
{
	TestGame game;
	return MainLoop::Run(Window::UpdateAllWindows);
}
