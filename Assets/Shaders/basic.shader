<----> defaults <---->
Bool useDiffuseTex false
Bool useSpecularTex false
Bool useShininessTex false
Bool useEmissiveTex false
Bool useNormalTex false

Vec4 diffuseBase {"x": 1,"y": 1,"z": 1,"w": 1}
Vec4 specularBase {"x": 1,"y": 1,"z": 1,"w": 1}
Float shininessBase 16
Vec4 emissiveBase {"x": 0,"y": 0,"z": 0,"w": 0}

TextureRef diffuseTex 0
TextureRef specularTex 0
TextureRef shininessTex 0
TextureRef normalTex 0
TextureRef emissiveTex 0

<----> vert <---->

#version 450
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 tangent;
layout (location = 3) in vec3 bitangent;
layout (location = 4) in vec3 color;
layout (location = 5) in vec2 uv;

uniform mat4 projectionMat = mat4(1.0);
uniform mat4 viewMat = mat4(1.0);
uniform mat4 modelMat = mat4(1.0);

out VSOut
{
	vec3 color;
	vec2 uv;
	vec3 fragPos;
	mat4 lightMat;
	mat3 tbn;
} vsOut;

void main()
{
	vsOut.color = color;
	vsOut.uv = uv;
	vsOut.fragPos = vec3(viewMat * modelMat * vec4(position, 1.0));
	vsOut.lightMat = viewMat;

	mat4 normalMat = transpose(inverse(viewMat * modelMat));
	vec3 t = normalize(vec3(normalMat * vec4(tangent,   0.0)));
	vec3 b = normalize(vec3(normalMat * vec4(bitangent, 0.0)));
	vec3 n = normalize(vec3(normalMat * vec4(normal,    0.0)));
	vsOut.tbn = mat3(t, b, n);

	gl_Position = projectionMat * viewMat * modelMat * vec4(position, 1);
} 

<----> frag <---->

#version 450

// ---- uniform ----
struct Material
{
	int transparency;
	vec4 diffuseBase;
	bool useDiffuseTex;
	sampler2D diffuseTex;

	vec4 specularBase;	
	bool useSpecularTex;
	sampler2D specularTex;

	float shininessBase;	
	bool useShininessTex;
	sampler2D shininessTex;
	
	bool useEmissiveTex;
	vec4 emissiveBase;
	sampler2D emissiveTex;

	bool useNormalTex;
	sampler2D normalTex;
};
uniform Material material;

struct AmbientLight
{
	vec4 color;
};
uniform AmbientLight ambientLight;

struct DirectionalLight
{
	vec4 color;
	vec3 direction;
	bool useShadow;
	sampler2D shadowDepth;
};
uniform DirectionalLight directionalLight;

struct PointLight
{
	vec4 color;
	vec3 position;
	bool useShadow;
	samplerCube shadowDepth;
};
uniform PointLight pointLight;

struct SpotLight
{
	vec4 color;
	vec3 position;
	vec3 direction;
	float innerCutOff;
	float outerCutOff;
	bool useShadow;
	sampler2D shadowDepth;
};
uniform SpotLight spotLight;


// ---- in ----
in VSOut
{
	vec3 color;
	vec2 uv;
	vec3 fragPos;
	mat4 lightMat;
	mat3 tbn;
} fsIn;


// ---- out ----
layout(location = 0) out vec4 outColor;


// ---- structs ----
struct MaterialData
{
	vec4 diffuse;
	vec4 specular;
	float shininess;
	vec4 emissive;
} materialData;

struct LightData
{
	vec4 diffuse;
	vec4 specular;
};


// ---- forward declerations ----
LightData CalcDirectionalLight(DirectionalLight light, vec3 normal, vec3 viewDir);
LightData CalcPointLight(PointLight light, vec3 normal, vec3 viewDir, vec3 fragPos);
LightData CalcSpotLight(SpotLight light, vec3 normal, vec3 viewDir, vec3 fragPos);


// ---- main ----
void main()
{
	// calc material properties
	if (material.useDiffuseTex)
	{
		materialData.diffuse = texture(material.diffuseTex, fsIn.uv) * material.diffuseBase;
	}
	else
	{
		materialData.diffuse = material.diffuseBase;
	}

	if (material.useSpecularTex)
	{
		materialData.specular = texture(material.specularTex, fsIn.uv) * material.specularBase;
	}
	else
	{
		materialData.specular = material.specularBase;
	}

	if (material.useShininessTex)
	{
		materialData.shininess = texture(material.shininessTex, fsIn.uv).r * material.shininessBase;
	}
	else
	{
		materialData.shininess = material.shininessBase;
	}

	if (material.useEmissiveTex)
	{
		materialData.emissive = texture(material.emissiveTex, fsIn.uv).r * material.emissiveBase;
	}
	else 
	{
		materialData.emissive = material.emissiveBase;
	}

	if (material.transparency == 1 && materialData.diffuse.a < 1.0)
	{
		discard;
	}
	float alpha;
	if (material.transparency == 2)
	{
		alpha = materialData.diffuse.w;
	}
	else 
	{
		alpha = 1;
	}


	// before light
	vec3 normal;
	if (material.useNormalTex)
	{
		normal = texture(material.normalTex, fsIn.uv).rgb;
	}
	else
	{
		normal = vec3(0.5, 0.5, 1);
	}
	normal = normal * 2.0 - 1.0;
	normal = normalize(fsIn.tbn * normal);

	vec3 viewDir = normalize(-fsIn.fragPos);

	LightData lightData = LightData(vec4(0), vec4(0));

	// emissive
	lightData.diffuse += materialData.emissive;

	// ambient light
	lightData.diffuse += ambientLight.color;

	LightData result;
	// directional light
	result = CalcDirectionalLight(directionalLight, normal, viewDir);
	lightData.diffuse += result.diffuse;
	lightData.specular += result.specular;

	// point light
	result = CalcPointLight(pointLight, normal, viewDir, fsIn.fragPos);
	lightData.diffuse += result.diffuse;
	lightData.specular += result.specular;

	// spot light
	result = CalcSpotLight(spotLight, normal, viewDir, fsIn.fragPos);
	lightData.diffuse += result.diffuse;
	lightData.specular += result.specular;

	// combine result
	lightData.diffuse *= materialData.diffuse;
	lightData.specular *= materialData.specular;
	outColor = vec4(vec3(lightData.diffuse + lightData.specular), alpha);
	//outColor = materialData.diffuse;
	//outColor = lightData.specular;
	//outColor = vec4(normal, 1);
	
	//float col = texture(frameBuffer, fsIn.uv).r;
	//if (col == 0) {discard;}
	//outColor = vec4(vec3(col), 1);
	//outColor = CalcPointLight(pointLight, normal, viewDir, fsIn.fragPos).diffuse;
}


// ---- functions ----
#define SHINE_EXP 8

LightData CalcDirectionalLight(DirectionalLight light, vec3 normal, vec3 viewDir)
{
	vec3 lightDir = normalize(-light.direction);
	// diffuse shading
	float diffuse = max(dot(normal, lightDir), 0.0);
	// specular shading
	vec3 halfwayDir = normalize(lightDir + viewDir);
	float specular = pow(max(dot(normal, halfwayDir), 0.0), materialData.shininess * SHINE_EXP);

	// combine results
	return LightData(
		 light.color * diffuse,
		 light.color * specular
	);
}

LightData CalcPointLight(PointLight light, vec3 normal, vec3 viewDir, vec3 fragPos)
{
	vec3 lightDir = normalize(light.position - fragPos);
	// diffuse shading
	float diffuse = max(dot(normal, lightDir), 0.0);
	// specular shading
	vec3 halfwayDir = normalize(lightDir + viewDir);
	float specular = pow(max(dot(normal, halfwayDir), 0.0), materialData.shininess * SHINE_EXP);
	// attenuation
	float distance = length(light.position - fragPos);
	float attenuation = 1.0 / pow(distance + 1, 2);

	// combine results
	return LightData(
		light.color * diffuse * attenuation,
		light.color * specular * attenuation
	);
}

LightData CalcSpotLight(SpotLight light, vec3 normal, vec3 viewDir, vec3 fragPos)
{
	vec3 lightDir = normalize(light.position - fragPos);
	// spot
	float theta = dot(lightDir, normalize(-light.direction));
	float epsilon = cos(light.innerCutOff) - cos(light.outerCutOff);
	float lightAmount = clamp((theta - cos(light.outerCutOff)) / epsilon, 0.0, 1.0);
	// diffuse shading
	float diffuse = max(dot(normal, lightDir), 0.0);
	// specular shading
	vec3 halfwayDir = normalize(lightDir + viewDir);
	float specular = pow(max(dot(normal, halfwayDir), 0.0), materialData.shininess * SHINE_EXP);
	// attenuation
	float distance = length(light.position - fragPos);
	float attenuation = 1.0 / pow(distance + 1, 2); // * pow(light.outerCutOff / (3.1415 * 2), 2)); // i think this should be 2*pi but the fallow is to small if it is

	// combine results
	return LightData(
		light.color * diffuse * attenuation * lightAmount,
		light.color * specular * attenuation * lightAmount
	);
}
