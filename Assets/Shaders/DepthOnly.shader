<----> defaults <---->

<----> vert <---->

#version 450

layout (location = 0) in vec3 position;

uniform mat4 projectionMat = mat4(1.0);
uniform mat4 viewMat = mat4(1.0);
uniform mat4 modelMat = mat4(1.0);

void main()
{
	gl_Position = projectionMat * viewMat * modelMat * vec4(position, 1);
} 

<----> frag <---->

#version 450

void main()
{
}
