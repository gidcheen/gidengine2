#pragma once

#include <iostream>
#include <vector>
#include <array>
#include <map>
#include <functional>

#include "Gid/Math/Maths.hpp"
#include "Utils/Logging.hpp"
#include "Utils/Callback.hpp"

using namespace std;
using namespace Gid::Maths;

#undef CreateWindow

struct GLFWwindow;
struct GLFWmonitor;

namespace Gid
{
class Window
{
public:
	enum class Mode
	{
		Minimized, Restored, Maximized
	};

	enum class CursorMode
	{
		Normal, Hidden, Captured
	};

	enum class Callbacks
	{
		Update, Position, Size, CursorPosition, MouseButton, Key, Char, ModeChange
	};

	using CallbackT = Callback<Window *>;

private:
	static constexpr unsigned maxButtons = 16;
	static constexpr unsigned maxKeys = 512;

	inline static Window * mainWindow = nullptr;
	inline static vector<Window *> allWindows;

	GLFWmonitor * glfwMonitor = nullptr;
	GLFWwindow * glfwWindow = nullptr;

	string title;
	bool isFullscreen;
	Mode mode = Mode::Restored;

	Vec2i pos;
	Vec2i size;

	Vec2f cursorPos;
	Vec2f lastCursorPos;
	CursorMode cursorMode = CursorMode::Normal;

	array<bool, maxButtons> mouseButtons;
	array<bool, maxButtons> lastMouseButtons;
	array<bool, maxKeys> keys;
	array<bool, maxKeys> lastKeys;

	map<Callbacks, CallbackT> callbacks;

	bool posInitalized = false;

public:
	explicit Window(const string & title, const Vec<int, 2> & size, Mode mode = Mode::Restored, bool isFullscreen = false);
	~Window();

	Window(const Window &) = delete;
	Window & operator=(const Window &) = delete;

	// get set
	static auto const GetMainWindow() { return mainWindow; }
	static void SetMainWindow(Window * w) { mainWindow = w; }
	static auto const GetAllWindows() { return allWindows; }

	string GetTitle() const { return title; }
	void SetTitle(const string & title);
	bool IsFullscreen() const { return isFullscreen; }
	void SetFullscreen(bool isFullscreen);
	Mode GetMode() const { return mode; }
	void SetMode(Mode mode);

	Vec2i GetPos() const { return pos; }
	void SetPos(const Vec2i & pos);
	Vec2i GetSize() const { return size; }
	void SetSize(const Vec2i & size);

	int GetWidth() const { return size.x; }
	void SetWidth(int width) { SetSize(Vec2i(width, size.y)); }
	int GetHeight() const { return size.y; }
	void SetHeight(int height) { SetSize(Vec2i(size.x, height)); }

	// todo: constrains

	Vec2f GetCursorPos() const { return cursorPos; }
	void SetCursorPos(const Vec2f & pos);
	void SetCursorToCenter() { SetCursorPos(Vec2f(roundf(size.x / 2.f), roundf(size.y / 2.f))); }
	Vec2f GetRelCursorPos() { return cursorPos - lastCursorPos; }

	CursorMode GetCursorMode() const { return cursorMode; }
	void SetCursorMode(CursorMode mode);

	CallbackT & GetCallback(Callbacks callback) { return callbacks[callback]; }
	auto AddCallback(Callbacks callback, CallbackT::Func const & func) { return callbacks[callback].Add(func); }

	void Open();
	void Close();

	// input
	// mouse
	bool IsMouseButtonPressed(unsigned button) { return mouseButtons.at(button); }
	bool IsMouseButtonDown(unsigned button) { return mouseButtons.at(button) && !lastKeys.at(button); }
	bool IsMouseButtonUp(unsigned button) { return !mouseButtons.at(button) && lastKeys.at(button); }

	// keyboard
	bool IsKeyPressed(unsigned keycode) { return keys.at(keycode); }
	bool IsKeyDown(unsigned keycode) { return keys.at(keycode) && !lastKeys.at(keycode); }
	bool IsKeyUp(unsigned keycode) { return !keys.at(keycode) && lastKeys.at(keycode); }
	// todo: get written chars

	bool ShouldClose() const;
	bool IsClosed() const { return !glfwWindow; }

	static bool UpdateAllWindows();

private:
	inline void UpdateWindow();
	inline void Clear();

	// callbacks
	static void GlfwErrorCallback(int error, const char * description)
	{
		LOG("GLFW error, code: " + to_string(error) + " description: " + description);
	}

	static void WindowPosCallback(GLFWwindow * window, int x, int y);
	static void WindowSizeCallback(GLFWwindow * window, int width, int height);
	static void CursorPosCallback(GLFWwindow * window, double x, double y);
	static void MouseButtonCallback(GLFWwindow * window, int button, int action, int mods);
	static void KeyCallback(GLFWwindow * window, int key, int scancode, int action, int mods);
	static void CharCallback(GLFWwindow * window, unsigned codePoint, int mods);

	static void MinimizeCallback(GLFWwindow * window, int iconified);
	static void MaximizeCallback(GLFWwindow * window, int maximized);
};
}
