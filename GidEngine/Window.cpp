#include "Window.hpp"

#include <algorithm>

#include "GL/GL.hpp"
#include <GLFW/glfw3.h>

using namespace Gid;

Window::Window(const string & title, const Vec<int, 2> & size, Mode mode, bool isFullscreen) :
	title(title),
	isFullscreen(isFullscreen),
	mode(mode),
	size(size)
{
	if (mainWindow == nullptr) { mainWindow = this; }
	if (allWindows.empty())
	{
		auto initSuccess = glfwInit();
		assert(initSuccess);
	}
	glfwMonitor = glfwGetPrimaryMonitor();
	assert(glfwMonitor);

	keys.fill(false);
	lastKeys.fill(false);
	mouseButtons.fill(false);
	lastMouseButtons.fill(false);

	allWindows.push_back(this);
}

Window::~Window()
{
	if (this == mainWindow) { mainWindow = nullptr; }
	auto & a = allWindows;
	a.erase(remove(a.begin(), a.end(), this), a.end());
	Close();
	if (allWindows.empty()) { glfwTerminate(); }
}

void Window::SetTitle(const string & title)
{
	this->title = title;
	if (!glfwWindow) { return; }
	glfwSetWindowTitle(glfwWindow, title.c_str());
}

void Window::SetFullscreen(bool isFullscreen)
{
	if (this->isFullscreen == isFullscreen) { return; }
	GLFWmonitor * monitor = isFullscreen ? glfwMonitor : nullptr;
	glfwSetWindowMonitor(glfwWindow, monitor, GetPos().x, GetPos().y, size.x, size.y, glfwGetVideoMode(glfwMonitor)->refreshRate);
}

void Window::SetMode(Mode mode)
{
	if (this->mode == mode) { return; }
	this->mode = mode;
	if (!glfwWindow) { return; }
	switch (mode)
	{
	case Window::Mode::Minimized: glfwIconifyWindow(glfwWindow);
		break;
	case Window::Mode::Restored: glfwRestoreWindow(glfwWindow);
		break;
	case Window::Mode::Maximized: glfwMaximizeWindow(glfwWindow);
		break;
	}
}

void Window::SetPos(Vec2i const & pos)
{
	this->pos = pos;
	if (!glfwWindow) { return; }
	glfwSetWindowPos(glfwWindow, pos.x, pos.y);
	callbacks[Callbacks::Position].Call(this);
}

void Window::SetSize(const Vec2i & size)
{
	this->size = size;
	if (!glfwWindow) { return; }
	glfwSetWindowSize(glfwWindow, size.x, size.y);
	callbacks[Callbacks::Size].Call(this);
}

void Window::SetCursorPos(Vec2f const & pos)
{
	cursorPos = pos;
	if (!glfwWindow) { return; }
	glfwSetCursorPos(glfwWindow, pos.x, pos.y);
	callbacks[Callbacks::CursorPosition].Call(this);
}

void Window::SetCursorMode(Window::CursorMode mode)
{
	cursorMode = mode;
	if (!glfwWindow) { return; }
	switch (mode)
	{
	case CursorMode::Normal: glfwSetInputMode(glfwWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		break;
	case CursorMode::Hidden: glfwSetInputMode(glfwWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
		break;
	case CursorMode::Captured:
#ifndef NDEBUG
		glfwSetInputMode(glfwWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
#else
		glfwSetInputMode(glfwWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
#endif
		SetCursorToCenter();
		lastCursorPos = cursorPos;
		break;
	}
}

void Window::Open()
{
	// window settings
	const GLFWvidmode * glfwMode = glfwGetVideoMode(glfwMonitor);
	glfwWindowHint(GLFW_RED_BITS, glfwMode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, glfwMode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, glfwMode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE, glfwMode->refreshRate);
	glfwWindowHint(GLFW_SAMPLES, 0);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4); // OpenGL 4.5
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifndef NDEBUG
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE); // set opengl debug to true
#endif

	GLFWmonitor * monitor = isFullscreen ? glfwMonitor : nullptr;
	GLFWwindow * glfwMainWindow = (mainWindow ? mainWindow->glfwWindow : nullptr);
	glfwWindow = glfwCreateWindow(GetWidth(), GetHeight(), title.c_str(), monitor, glfwMainWindow);
	assert(glfwWindow);
	glfwSetWindowUserPointer(glfwWindow, this);

	glfwMakeContextCurrent(glfwWindow);
	glfwSwapInterval(0); // disable vsync // doesnt seem to work for mesa amd on linux
	GL::GL::Init();
	Clear();
	glfwSwapBuffers(glfwWindow);

	// window callback
	glfwSetWindowPosCallback(glfwWindow, WindowPosCallback);
	glfwSetWindowSizeCallback(glfwWindow, WindowSizeCallback);
	glfwSetKeyCallback(glfwWindow, KeyCallback);
	glfwSetCharModsCallback(glfwWindow, CharCallback);
	glfwSetMouseButtonCallback(glfwWindow, MouseButtonCallback);
	glfwSetCursorPosCallback(glfwWindow, CursorPosCallback);

	glfwSetWindowIconifyCallback(glfwWindow, MinimizeCallback);
	glfwSetWindowMaximizeCallback(glfwWindow, MaximizeCallback);

	if (posInitalized)
	{
		SetPos(pos);
	}
	else
	{
		Vec2i initialPos;
		glfwGetWindowPos(glfwWindow, &initialPos.x, &initialPos.y);
		pos = initialPos;
		posInitalized = true;
	}
	SetCursorMode(cursorMode);

	SetMode(mode);
}

void Window::Close()
{
	if (!glfwWindow) { return; }
	glfwDestroyWindow(glfwWindow);
	glfwWindow = nullptr;
}

bool Window::ShouldClose() const
{
	return !glfwWindow || glfwWindowShouldClose(glfwWindow) != 0;
}

bool Window::UpdateAllWindows()
{
	auto mainWindow = Window::GetMainWindow();
	if (mainWindow == nullptr || mainWindow->IsClosed()) { return false; }

	glfwPollEvents();
	for (auto w : allWindows)
	{
		if (!w->IsClosed() && w->ShouldClose())
		{
			w->Close();
		}
		else
		{
			w->UpdateWindow();
		}
	}

	return true;
}

void Window::UpdateWindow()
{
	if (!glfwWindow) { return; }

	glfwMakeContextCurrent(glfwWindow);
	Clear();
	callbacks[Callbacks::Update].Call(this);
	glfwSwapBuffers(glfwWindow);

	lastKeys = keys;
	lastMouseButtons = mouseButtons;
	if (cursorMode == CursorMode::Captured)
	{
		// workaround for broken cursor callback
		auto shouldCenter = cursorPos.x <= 100 || cursorPos.x >= size.x - 100 || cursorPos.y <= 100 || cursorPos.y >= size.y - 100;
		if (shouldCenter) { SetCursorToCenter(); }
	}
	lastCursorPos = cursorPos;
}

void Window::Clear()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void Window::WindowPosCallback(GLFWwindow * window, int x, int y)
{
	auto * w = (Window *)glfwGetWindowUserPointer(window);
	w->pos = { x, y };
	w->callbacks[Callbacks::Position].Call(w);
}

void Window::WindowSizeCallback(GLFWwindow * window, int width, int height)
{
	auto * w = (Window *)glfwGetWindowUserPointer(window);
	w->size = { width, height };
	w->callbacks[Callbacks::Size].Call(w);
}

void Window::CursorPosCallback(GLFWwindow * window, double x, double y)
{
	auto * w = (Window *)glfwGetWindowUserPointer(window);
	w->cursorPos = { (float)x, (float)y };
	w->callbacks[Callbacks::CursorPosition].Call(w);
}

void Window::MouseButtonCallback(GLFWwindow * window, int button, int action, int mods)
{
	auto * w = (Window *)glfwGetWindowUserPointer(window);
	w->mouseButtons[button] = action != GLFW_RELEASE;
	w->callbacks[Callbacks::MouseButton].Call(w);
}

void Window::KeyCallback(GLFWwindow * window, int key, int scancode, int action, int mods)
{
	auto * w = (Window *)glfwGetWindowUserPointer(window);
	w->keys[key] = action != GLFW_RELEASE;
	w->callbacks[Callbacks::Key].Call(w);
}

void Window::CharCallback(GLFWwindow * window, unsigned codePoint, int mods)
{
	auto * w = (Window *)glfwGetWindowUserPointer(window);
	w->callbacks[Callbacks::Char].Call(w);
}

void Window::MinimizeCallback(GLFWwindow * window, int iconified)
{
	auto * w = (Window *)glfwGetWindowUserPointer(window);
	w->SetMode(iconified ? Mode::Minimized : Mode::Restored);
	w->callbacks[Callbacks::ModeChange].Call(w);
}

void Window::MaximizeCallback(GLFWwindow * window, int maximized)
{
	auto * w = (Window *)glfwGetWindowUserPointer(window);
	w->SetMode(maximized ? Mode::Maximized : Mode::Restored);
	w->callbacks[Callbacks::ModeChange].Call(w);
}
