#include "Logging.hpp"
#include "StringUtils.hpp"

void Logging::Log(string const & msg, LogData const & logData, Type type)
{
	string typeString;
	switch (type)
	{
	case Type::Log: typeString = "LOG";
		break;
	case Type::Warning: typeString = "WARNING";
		break;
	case Type::Error: typeString = "ERROR";
		break;
	}

	string log = "\n";
	log += typeString + " >>----  ------\n";
	log += "\n" + msg + "\n\n";
	log += "in File: " + RemovePath(logData.file) + ", Line: " + to_string(logData.line) + "\n";
	log += "------  ----<< " + typeString;
	(type == Type::Error ? cerr : cout) << log << endl;
}

string Logging::RemovePath(const string & str)
{
	string s = str;
	s = StringUtils::FixPath(s);
	s = StringUtils::SplitString(s, '/').back();
	return s;
}
