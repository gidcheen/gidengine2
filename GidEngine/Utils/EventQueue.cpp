//
// Created by gidcheen on 11/16/18.
//

#include "EventQueue.hpp"

using namespace Gid;

void EventQueue::Enqueue(function<void()> const & event)
{
	lock_guard lock(mut);
	events.push(event);
}

void EventQueue::ExecuteOne()
{
	Execute(1);
}

void EventQueue::ExecuteAll()
{
	Execute(events.size());
}

void EventQueue::Execute(size_t count)
{
	lock_guard lock(mut);
	for (size_t i = 0; i < count; i++)
	{
		if (events.empty()) { break; }
		auto event = events.front();
		event();
		events.pop();
	}
}
