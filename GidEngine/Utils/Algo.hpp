#pragma once

#include <algorithm>
#include <memory>

using namespace std;

template<typename T>
struct PtrValComp
{
	bool operator()(T const & a, T const & b) const
	{
		if (!a || !b) { return a < b; }
		return *a < *b;
	}
};

template<typename T>
class SharedPtrPtrComp
{
private:
	T const * ptr;
public:
	explicit SharedPtrPtrComp(T const * ptr) : ptr(ptr) {}
	bool operator()(shared_ptr<T> const & sharedPtr) { return sharedPtr.get() == ptr; }
};

template<typename T>
class PtrSharedPtrComp
{
private:
	shared_ptr<T const> sharedPtr{};
public:
	explicit PtrSharedPtrComp(shared_ptr<T const> & sharedPtr) : sharedPtr(sharedPtr) {}
	bool operator()(T const * ptr) { return sharedPtr.get() == ptr; }
};


template<typename CollectionT, typename ItemT>
void Erase(CollectionT & collection, ItemT const & item)
{
	collection.erase(remove(collection.begin(), collection.end(), item), collection.end());
};

template<typename CollectionT, typename PredT>
void EraseIf(CollectionT & collection, PredT const & pred)
{
	collection.erase(remove_if(collection.begin(), collection.end(), pred), collection.end());
};

template<typename CollectionT, typename CollectionFromT>
void EraseAll(CollectionT & collection, CollectionFromT const & collectionFrom)
{
	auto start = collection.end();
	for (auto const & item : collectionFrom)
	{
		start = remove(collection.begin(), start(), item);
	}
	collection.erase(start, collection.end());
};


template<typename CollectionT, typename ItemT>
void EraseFirst(CollectionT & collection, ItemT const & item)
{
	auto it = find(collection.begin(), collection.end(), item);
	if (it != collection.end()) { collection.erase(it); }
};

template<typename CollectionT, typename PredT>
void EraseIfFist(CollectionT & collection, PredT const & pred)
{
	auto it = find_if(collection.begin(), collection.end(), pred);
	if (it != collection.end()) { collection.erase(it); }
};

template<typename CollectionT, typename CollectionFromT>
void EraseAllFirst(CollectionT & collection, CollectionFromT const & collectionFrom)
{
	for (auto const & item : collectionFrom) { EraseFirst(collection, item); }
};


