#pragma once

#include <thread>
#include <map>

#include "EventQueue.hpp"

using namespace std;

namespace Gid
{
class PerThreadEventQueues
{
private:
	inline static map<thread::id, EventQueue> threadEvents;
public:
	static EventQueue & Get();
	static EventQueue & Get(thread::id id);
};
}
