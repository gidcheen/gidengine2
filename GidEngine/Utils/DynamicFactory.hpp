#pragma once

#include <memory>
#include <map>
#include <functional>

#include "Utils/Name.hpp"
#include "Utils/Assert.hpp"

using namespace std;

template<typename... Args>
struct ConstructorCreator
{
	template<typename T>
	struct Creator
	{
		T * operator()(Args const & ... args) const { return T(args...); }
	};
};

template<typename Key, typename Type, typename Creator, typename... Args>
class DynamicFactory
{
private:
	map<Key, function<Type *(Args...)>> elements;

public:
	template<typename T, typename C =  Creator>
	void Add(Key const & key, typename C::template Creator<T> creator = typename C::template Creator<T>())
	{
		elements[key] = [creator](Args const & ... args)
		{
			T * created = creator(args...);
			auto * ret = dynamic_cast<Type *>(created);
			if (!ret) { delete created; }
			return ret;
		};
	}

	Type * Create(Key const & name, Args const & ... args) { return elements.find(name) != elements.end() ? elements[name](args...) : nullptr; }
};
