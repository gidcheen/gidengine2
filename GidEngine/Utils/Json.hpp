#pragma once

#include <string>
#include <sstream>
#include <json/json.h>

using namespace std;
using JsonV = Json::Value;

inline JsonV stoj(string const & str)
{
	JsonV ret;
	stringstream stream(str);

	Json::CharReaderBuilder builder;
	builder["collectComments"] = true;

	string errors;
	Json::parseFromStream(builder, stream, &ret, &errors);
	return ret;
}

inline string jtos(JsonV const & json)
{
	Json::StreamWriterBuilder builder;
	builder["indentation"] = "\t";
	return Json::writeString(builder, json);
}
