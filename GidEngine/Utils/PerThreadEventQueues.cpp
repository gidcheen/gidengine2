#include "PerThreadEventQueues.hpp"

using namespace Gid;

EventQueue & PerThreadEventQueues::Get()
{
	return Get(this_thread::get_id());
}

EventQueue & PerThreadEventQueues::Get(thread::id id)
{
	return threadEvents[id];
}
