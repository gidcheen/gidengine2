#pragma once

#include <memory>
#include <list>
#include <functional>

using namespace std;

template<typename... T>
class Callback // todo: invalidate handles on destruction
{
public:
	using Func = function<void(T...)>;

private:
	using FuncList = list<Func>;
	using FuncListIterator = typename FuncList::iterator;

public:
	struct Handle
	{
		Callback<T...> * callback;
		FuncListIterator const handle;
		Handle(Callback<T...> * callback, FuncListIterator const & handle) : callback(callback), handle(handle) {}
		~Handle() { callback->Remove(*this); }
	};

private:
	FuncList callbacks;

public:
	shared_ptr<Handle const> Add(Func const & cb)
	{
		callbacks.push_back(cb);
		return shared_ptr<Handle const>(new Handle(this, --callbacks.end()));
	}

	void Call(T... args) const
	{
		for (auto & f : callbacks)
		{
			f(args...);
		}
	}

private:
	void Remove(Handle const & handle) { callbacks.erase(handle.handle); }
};

template<typename... T>
using CallbackHandle = shared_ptr<typename Callback<T...>::Handle const>;

template<typename... T>
using CbHandle = CallbackHandle<T...>;
