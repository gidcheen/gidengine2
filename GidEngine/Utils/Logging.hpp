#pragma once

#include <string>
#include <iostream>

using namespace std;

#define LOG_DATA LogData { __FILE__, __LINE__ }

#define LOG(MSG) Logging::Log(MSG, LOG_DATA)
#define LOG_WARNING(MSG) Logging::Log(MSG, LOG_DATA, Logging::Type::Warning)
#define LOG_ERROR(MSG) Logging::Log(MSG, LOG_DATA, Logging::Type::Error)

struct LogData
{
	string file;
	int line;
};

class Logging
{
public:
	enum class Type
	{
		Log, Warning, Error
	};

	static void Log(const string & msg, LogData const & logData, Type type = Type::Log);

private:
	static string RemovePath(const string & str);
};
