#include "UId.hpp"

#include <random>

UId UId::Next()
{
	mt19937_64 gen(random_device{}());
	uniform_int_distribution<Id> distribution;
	return UId(distribution(gen), distribution(gen));
}
