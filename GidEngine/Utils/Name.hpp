#pragma once

#include <json/json.h>
#include "Hash.hpp"

class Name
{
private:
	string str;
	Id id;

public:
	Name() : Name("") {};
	template<unsigned c>
	constexpr explicit Name(char const (& str)[c]) : str(str), id(Hash::Get(str)) {}
	explicit Name(string const & str) : str(str), id(Hash::Get(str)) {}
	explicit Name(Json::Value const & json) : Name(json.asString()) {}

	Name & operator=(char const * other) { return *this = string(other); }
	Name & operator=(string const & other)
	{
		*this = Name(other);
		return *this;
	}
	Name & operator=(Name const & other)
	{
		this->str = other.str;
		this->id = other.id;
		return *this;
	}

	operator Id() const { return GetId(); }
	operator string() const { return GetString(); }

	bool operator==(Name const & other) const { return id == other.id; }
	bool operator!=(Name const & other) const { return id != other.id; }
	bool operator==(string const & other) const { return *this == Name(other); }
	bool operator!=(string const & other) const { return !(*this == other); }
	bool operator<(Name const & other) const { return id < other.id; }

	Id GetId() const { return id; }
	string const & GetString() const { return str; }
	Json::Value ToJson() { return Json::Value(str); }
};
