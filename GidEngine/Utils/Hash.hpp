#pragma once

#include <string>

using Id = unsigned long long;

using namespace std;

class Hash
{
private:
	static constexpr Id start = 619;
	static constexpr Id prime = 101;

public:
	static Id Get(const string & str)
	{
		Id hash = start;
		for (auto c : str)
		{
			hash = hash * prime + c;
		}
		return hash;
	}

	template<unsigned c>
	static constexpr Id Get(char const (&str)[c])
	{
		Id hash = start;
		for (unsigned i = 0; i < c; i++)
		{
			hash = hash * prime + str[i];
		}
		return hash;
	}
};
