#pragma once

#include <string>
#include <typeinfo>
#include "Name.hpp"

#if defined(__GNUG__)

#include <cxxabi.h>

#endif

using namespace std;

inline string Demangle(string const & typeName)
{
#if defined(__GNUG__)
	int result;
	auto demangled = abi::__cxa_demangle(typeName.c_str(), nullptr, nullptr, &result);
	string ret(demangled);
	free(demangled);
	return ret;
#else
	return typeName;
#endif
}

inline Name TypeName(type_info const & typeInfo)
{
	Name ret(Demangle(string(typeInfo.name())));
	return ret;
}

template<typename T>
inline Name TypeName()
{
	static Name ret(TypeName(typeid(T)));
	return ret;
}

template<typename T>
inline Name TypeName(T const & t)
{
	if constexpr (is_polymorphic_v<T>)
	{
		return TypeName(typeid(t));
	}
	else
	{
		return TypeName<T>();
	}
}