#pragma once

#include <string>
#include <cassert>

#include "Utils/Json.hpp"
#include "Hash.hpp"
#include "StringUtils.hpp"

using namespace std;

class UId
{
private:
	Id a = 0;
	Id b = 0;

	string str;
	JsonV json;

private:
	UId(Id a, Id b) : a(a), b(b), str(GenerateString()), json(GenerateJson()) {}

public:
	explicit UId(char const * str) : UId(FromString(str)) {}
	explicit UId(string const & str) : UId(FromString(str)) {}
	explicit UId(JsonV const & json) : UId(json.asString()) {}

	UId(UId const & other)
	{
		a = other.a;
		b = other.b;
		str = other.str;
		json = other.json;
	}

	bool operator==(UId const & other) const { return a == other.a && b == other.b; }
	bool operator!=(UId const & other) const { return !(*this == other); }
	bool operator<(UId const & other) const { return a != other.a ? a < other.a : b < other.b; }

	string const & ToString() const { return str; }
	JsonV ToJson() const { return json; }

	static UId FromString(const string & str)
	{
		auto strings = StringUtils::SplitString(str, '-');
		return UId(stoull(strings[0]), stoull(strings[1]));
	}

	bool IsNull() const { return a == 0 && b == 0; }

	static UId Next();
	static UId Null() { return UId(0, 0); }

private:
	string GenerateString() { return to_string(a) + "-" + to_string(b); }
	JsonV GenerateJson() { return str; }
};

inline string to_string(UId const & uId) { return uId.ToString(); }
