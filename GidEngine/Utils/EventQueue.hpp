#pragma once

#include <queue>
#include <functional>
#include <mutex>

using namespace std;

namespace Gid
{
class EventQueue
{
private:
	queue<function<void()>> events;
	mutex mut;

public:
	void Enqueue(function<void()> const & event);
	void ExecuteOne();
	void ExecuteAll();
	void Execute(size_t count);
};
}
