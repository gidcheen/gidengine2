#pragma once

#include <vector>
#include <string>
#include <algorithm>

#include <filesystem>

using namespace std;
namespace fs = std::filesystem;

class StringUtils
{
public:
	static string ToUpper(const string & s)
	{
		string ret = s;
		transform(ret.begin(), ret.end(), ret.begin(), ::toupper);
		return ret;
	}

	static string ToLower(const string & s)
	{
		string ret = s;
		transform(ret.begin(), ret.end(), ret.begin(), ::tolower);
		return ret;
	}

	static vector<string> SplitString(const string & src, char delimiter)
	{
		vector<string> ret;
		string currentS;
		for (char c : src)
		{
			if (c == delimiter)
			{
				if (currentS.empty()) { continue; }
				ret.push_back(currentS);
				currentS = "";
				continue;
			}
			currentS += c;
		}
		if (!currentS.empty()) { ret.push_back(currentS); };
		return ret;
	}

	static vector<string> SplitString(const string & src, const string & delimiter)
	{
		vector<string> ret;
		string s = src;
		size_t pos = 0;
		while ((pos = s.find(delimiter)) != string::npos)
		{
			if (pos != 0) { ret.push_back(s.substr(0, pos)); }
			s.erase(0, pos + delimiter.length());
		}
		ret.push_back(s);
		return ret;
	}

	static string FixPath(string & ps)
	{
		replace(ps.begin(), ps.end(), '\\', '/');
		if (ps.front() == '\"' && ps.back() == '\"')
		{
			ps.erase(ps.begin());
			ps.pop_back();
		}
		return ps;
	}

	static string GetFileEnding(const string & path)
	{
		auto start = path.find_last_of('.');
		if (start == string::npos) { return ""; }
		string ending(path.begin() + start + 1, path.end());
		ending = StringUtils::ToLower(ending);
		return ending;
	}

	static fs::path ToPath(const string & s)
	{
		return fs::path() += s;
	}
};
