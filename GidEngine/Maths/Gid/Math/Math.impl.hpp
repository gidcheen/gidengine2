#include "Math.hpp"
#include "Mat.hpp"
#include "Quat.hpp"

namespace Gid
{
namespace Maths
{
template<typename T>
Mat<T, 4, 4> Math::Orthographic(T left, T right, T bottom, T top, T near, T far)
{
	Mat<T, 4, 4> ret;

	ret[0][0] = 2.0f / (right - left);
	ret[1][1] = 2.0f / (top - bottom);
	ret[2][2] = -2.0f / (far - near);

	ret[3][0] = -(right + left) / (right - left);
	ret[3][1] = -(top + bottom) / (top - bottom);
	ret[3][2] = -(far + near) / (far - near);
	ret[3][3] = 1;

	return ret;
}

template<typename T>
Mat<T, 4, 4> Math::Perspective(T fov, T apsect, T near, T far)
{
	Mat<T, 4, 4> ret;

	float q = 1.0f / tanf(ToRadians(0.5f * fov));
	float a = q / apsect;
	float b = -(far + near) / (far - near);
	float c = -(2.0f * far * near) / (far - near);

	ret[0][0] = a;
	ret[1][1] = q;
	ret[2][2] = b;
	ret[2][3] = -1.0f;
	ret[3][2] = c;

	return ret;
}

template<typename T>
Mat<T, 4, 4> Math::Translation(const Mat<T, 3, 1> & translation)
{
	auto ret = Mat<T, 4, 4>::Identity();
	ret[3][0] = translation.x;
	ret[3][1] = translation.y;
	ret[3][2] = translation.z;
	return ret;
}

template<typename T>
Mat<T, 4, 4> Math::Rotation(const Quat<T> & quat)
{
	auto ret = Mat<T, 4, 4>::Identity();

	auto x = quat.v.x;
	auto y = quat.v.y;
	auto z = quat.v.z;
	auto w = quat.w;

	auto x2 = x * x;
	auto y2 = y * y;
	auto z2 = z * z;

	ret[0][0] = 1 - 2 * y2 - 2 * z2;
	ret[0][1] = 2 * x * y + 2 * z * w;
	ret[0][2] = 2 * x * z - 2 * y * w;

	ret[1][0] = 2 * x * y - 2 * z * w;
	ret[1][1] = 1 - 2 * x2 - 2 * z2;
	ret[1][2] = 2 * y * z + 2 * x * w;

	ret[2][0] = 2 * x * z + 2 * y * w;
	ret[2][1] = 2 * y * z - 2 * x * w;
	ret[2][2] = 1 - 2 * x2 - 2 * y2;

	return ret;
}

template<typename T>
Mat<T, 4, 4> Math::Scale(const Mat<T, 3, 1> & scale)
{
	auto ret = Mat<T, 4, 4>::Identity();
	ret[0][0] = scale.x;
	ret[1][1] = scale.y;
	ret[2][2] = scale.z;
	return ret;
}

template<typename T>
Mat<T, 4, 4> Math::Transform(Mat<T, 3, 1> const & position, Quat<T> const & rotation, Mat<T, 3, 1> const & scale)
{
	return Math::Translation(position) * Math::Rotation(rotation) * Math::Scale(scale);
}

template<typename T>
Mat<T, 3, 1> Math::MatToPosition(const Mat<T, 4, 4> & mat)
{
	return Mat<T, 3, 1>(mat[3][0], mat[3][1], mat[3][2]);
}

template<typename T>
Quat<T> Math::MatToRotation(Mat<T, 4, 4> const & mat)
{
	Quat<T> ret;

	ret.w = sqrt(max(T(0), 1 + mat[0][0] + mat[1][1] + mat[2][2])) / 2;
	ret.v.x = sqrt(max(T(0), 1 + mat[0][0] - mat[1][1] - mat[2][2])) / 2;
	ret.v.y = sqrt(max(T(0), 1 - mat[0][0] + mat[1][1] - mat[2][2])) / 2;
	ret.v.z = sqrt(max(T(0), 1 - mat[0][0] - mat[1][1] + mat[2][2])) / 2;

	ret.v.x = copysign(ret.v.x, mat[1][2] - mat[2][1]);
	ret.v.y = copysign(ret.v.y, mat[2][0] - mat[0][2]);
	ret.v.z = copysign(ret.v.z, mat[0][1] - mat[1][0]);

	return ret;
}

template<typename T>
Mat<T, 3, 1> Math::MatToScale(const Mat<T, 4, 4> & mat)
{
	return Mat<T, 3, 1>(mat[0][0], mat[1][1], mat[2][2]);
}

template<typename T>
Mat<T, 3, 1> Math::QuatToEuler(Quat<T> const & q)
{
	Mat<T, 3, 1> ret;

	T sinx = 2.0 * (q.w * q.v.x + q.v.y * q.v.z);
	T cosx = 1.0 - 2.0 * (q.v.x * q.v.x + q.v.y * q.v.y);
	ret.x = atan2(sinx, cosx);

	T siny = +2.0 * (q.w * q.v.y - q.v.z * q.v.x);
	ret.y = fabs(siny) >= 1 ? ret.y = copysign(Math::PI / 2, siny) : ret.y = asin(siny);

	T sinz = +2.0 * (q.w * q.v.z + q.v.x * q.v.y);
	T cosz = +1.0 - 2.0 * (q.v.y * q.v.y + q.v.z * q.v.z);
	ret.z = atan2(sinz, cosz);

	return ret;
}

template<typename T>
Quat<T> Math::EulerToQuat(Mat<T, 3, 1> const & vec)
{
	return Quat<T>(Vec3f(0, 0, 1), vec.x) * Quat<T>(Vec3f(0, 1, 0), vec.x) * Quat<T>(Vec3f(1, 0, 0), vec.x);
}
}
}

#include "Mat.hpp"
