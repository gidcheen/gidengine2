#pragma once

#include <cmath>
#include <array>
#include <string>
#include <type_traits>

#include "MatData.hpp"
#include "MatTypedef.hpp"

#define ENABLE(x) typename = typename enable_if<x>::type

using namespace std;

namespace Gid
{
namespace Maths
{
template<typename T, unsigned X, unsigned Y>
struct Mat : public MatData<T, X, Y>
{
	static_assert(is_fundamental_v<T>, "only basic types allowed");

	Mat() { this->Clear(); }

	// ---- set
	explicit Mat(const T * values) { this->Set(values); }
	template<unsigned X2, unsigned Y2>
	explicit Mat(const Mat<T, X2, Y2> & other) { this->Set(other); }

	Mat<T, X, Y> & operator=(const T * values) { return this->Set(values); }
	template<unsigned X2, unsigned Y2>
	Mat<T, X, Y> & operator=(const Mat<T, X2, Y2> & other) { return this->Clear().Set(other); }

	Mat<T, X, Y> & Set(const T * values);
	template<unsigned X2, unsigned Y2>
	Mat<T, X, Y> & Set(const Mat<T, X2, Y2> & other);
	// ---- end set

	// operators
	array<T, X> & operator[](unsigned index) { return this->columns.at(index); }
	array<T, X> const & operator[](unsigned index) const { return this->columns.at(index); }

	template<unsigned M2Y>
	Mat<T, X, M2Y> operator*(const Mat<T, Y, M2Y> & other) const;

	Mat<T, X, Y> operator+(const Mat<T, X, Y> & other) const { return Mat<T, X, Y>(*this) += other; }
	Mat<T, X, Y> operator-(const Mat<T, X, Y> & other) const { return Mat<T, X, Y>(*this) -= other; }
	Mat<T, X, Y> operator*(const T & other) const { return Mat<T, X, Y>(*this) *= other; }
	Mat<T, X, Y> operator/(const T & other) const { return Mat<T, X, Y>(*this) /= other; }

	Mat<T, X, Y> & operator+=(const Mat<T, X, Y> & other);
	Mat<T, X, Y> & operator-=(const Mat<T, X, Y> & other);
	Mat<T, X, Y> & operator*=(const T & other);
	Mat<T, X, Y> & operator/=(const T & other) { return *this *= 1 / other; }

	bool operator==(const Mat<T, X, Y> & other) const;
	bool operator!=(const Mat<T, X, Y> & other) const { return !(*this == other); }

	//
	Mat<T, X, Y> & Fill(const T & v);
	Mat<T, X, Y> & SetDiagonale(const T & value);
	Mat<T, X, Y> & Clear() { return this->Fill(T()); }

	Mat<T, Y, X> Transposed() const;

	Mat<T, X - 1, Y - 1> GetMinor(unsigned indexX, unsigned indexY);

	T GetTrace();

	Mat<T, X, Y> Min(const Mat<T, X, Y> & other) const { return this->Min(*this, other); }
	static Mat<T, X, Y> Min(const Mat<T, X, Y> & a, const Mat<T, X, Y> & b);
	Mat<T, X, Y> Max(const Mat<T, X, Y> & other) const { return this->Max(*this, other); }
	static Mat<T, X, Y> Max(const Mat<T, X, Y> & a, const Mat<T, X, Y> & b);

	static Mat<T, X, Y> CompMult(const Mat<T, X, Y> & a, const Mat<T, X, Y> & b);

	static Mat<T, X, Y> Diagonal(const T & diagonal) { return Mat<T, X, Y>().SetDiagonale(diagonal); }

	static constexpr Mat<T, X, Y> Identity() { return Mat<T, X, Y>::Diagonal(1); }

	string ToString() const;

	// square matrix
	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE(X2 == Y2)>
	auto & Transpose();

	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE((X2 == Y2) && (X2 == 1))>
	T GetDeterminante() { return this->elements[0]; }
	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE((X2 == Y2) && (X2 > 1))>
	T GetDeterminante(int = 0);

	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE((X2 == Y2) && (X2 != 4))>
	Mat<T, X, X> & Invert();
	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE((X2 == Y2) && (X2 == 4))>
	Mat<T, X, X> & Invert(int = 0);
	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE(X2 == Y2)>
	Mat<T, X, X> Inverse() const { return Mat<T, X, X>(*this).Invert(); }

	// vec
	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE((Y2 == 1) && (X2 > 1))>
	T Lenght() const { return sqrt(this->LenghtSquared()); }
	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE((Y2 == 1) && (X2 > 1))>
	T LenghtSquared() const;
	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE((Y2 == 1) && (X2 > 1))>
	Mat<T, X, Y> & Normalize() { return *this /= Lenght(); }
	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE((Y2 == 1) && (X2 > 1))>
	Mat<T, X, Y> Normalized() const { return Mat<T, X, 1>(*this).Normalize(); }

	// vec2
	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE((Y2 == 1) && (X2 == 2))>
	Mat(const T & x, const T & y)
	{
		this->x = x;
		this->y = y;
	}

	// vec3
	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE((Y2 == 1) && (X2 == 3))>
	Mat(const T & x, const T & y, const T & z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}
	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE((Y2 == 1) && (X2 == 3))>
	Mat(const Mat<T, 2, 1> & v, const T & z)
	{
		this->x = v.x;
		this->y = v.y;
		this->z = z;
	}
	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE((Y2 == 1) && (X2 == 3))>
	Mat<T, X, Y> Cross(Mat<T, X, Y> const & other) const;
	template<unsigned X2 = X, unsigned Y2 = Y, ENABLE((Y2 == 1) && (X2 == 3))>
	T Dot(Mat<T, X, Y> const & other) const { return (*reinterpret_cast<const Mat<T, Y, X> *>(&other) * *this).x; }

	// vec4
	Mat(const T & x, const T & y, const T & z, const T & w)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}
	Mat(const Mat<T, 2, 1> & v, const T & z, const T & w)
	{
		this->x = v.x;
		this->y = v.y;
		this->z = z;
		this->w = w;
	}
	Mat(const Mat<T, 3, 1> & v, const T & w)
	{
		this->x = v.x;
		this->y = v.y;
		this->z = v.z;
		this->w = w;
	}
	Mat(const Mat<T, 2, 1> & v1, const Mat<T, 2, 1> & v2)
	{
		this->x = v1.x;
		this->y = v1.y;
		this->z = v2.x;
		this->w = v2.y;
	}
};
}
}

#include "Mat.impl.hpp"
