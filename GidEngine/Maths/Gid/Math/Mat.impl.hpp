#pragma once

#include "Mat.hpp"

#include <cassert>

#include "Math.hpp"

#undef near
#undef far

namespace Gid
{
namespace Maths
{
class Math;

template<typename T, unsigned X, unsigned Y>
Mat<T, X, Y> & Mat<T, X, Y>::Set(const T * values)
{
	assert(values);
	for (unsigned i = 0; i < X * Y; ++i)
	{
		this->elements[i] = values[i];
	}
	return *this;
}

template<typename T, unsigned X, unsigned Y>
template<unsigned X2, unsigned Y2>
Mat<T, X, Y> & Mat<T, X, Y>::Set(const Mat<T, X2, Y2> & other)
{
	for (unsigned i = 0; i < (Y < Y2 ? Y : Y2); ++i)
	{
		for (unsigned j = 0; j < (X < X2 ? X : X2); ++j)
		{
			this->columns[i][j] = other.columns[i][j];
		}
	}
	return *this;
}

template<typename T, unsigned X, unsigned Y>
template<unsigned M2Y>
Mat<T, X, M2Y> Mat<T, X, Y>::operator*(const Mat<T, Y, M2Y> & other) const
{
	Mat<T, X, M2Y> ret;
	for (unsigned row = 0; row < X; ++row)
	{
		for (unsigned col = 0; col < M2Y; ++col)
		{
			for (unsigned el = 0; el < Y; ++el)
			{
				ret[col][row] += this->columns[el][row] * other[col][el];
			}
		}
	}
	return ret;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, X, Y> & Mat<T, X, Y>::operator+=(const Mat<T, X, Y> & other)
{
	for (unsigned i = 0; i < X * Y; ++i)
	{
		this->elements[i] += other.elements[i];
	}
	return *this;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, X, Y> & Mat<T, X, Y>::operator-=(const Mat<T, X, Y> & other)
{
	for (unsigned i = 0; i < X * Y; ++i)
	{
		this->elements[i] -= other.elements[i];
	}
	return *this;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, X, Y> & Mat<T, X, Y>::operator*=(const T & other)
{
	for (unsigned i = 0; i < X * Y; ++i)
	{
		this->elements[i] *= other;
	}
	return *this;
}

template<typename T, unsigned X, unsigned Y>
bool Mat<T, X, Y>::operator==(const Mat<T, X, Y> & other) const
{
	for (int i = 0; i < Y * Y; ++i)
	{
		if (this->elements[i] != other.elements[i]) { return false; }
	}
	return true;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, X, Y> & Mat<T, X, Y>::Fill(const T & v)
{
	this->elements.fill(v);
	return *this;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, X, Y> & Mat<T, X, Y>::SetDiagonale(const T & value)
{
	for (unsigned i = 0; i < X * Y; i += X + 1)
	{
		this->elements[i] = value;
	}
	return *this;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, Y, X> Mat<T, X, Y>::Transposed() const
{
	Mat<T, Y, X> ret;
	for (unsigned i = 0; i < Y; ++i)
	{
		for (unsigned j = 0; j < X; ++j)
		{
			ret.columns[j][i] = this->columns[i][j];
		}
	}
	return ret;
}

template<typename T, unsigned X, unsigned Y>
Mat<T, X - 1, Y - 1> Mat<T, X, Y>::GetMinor(unsigned indexX, unsigned indexY)
{
	Mat<T, X - 1, Y - 1> ret;
	int skipY = 0;
	for (int i = 0; i < Y - 1; ++i)
	{
		skipY += (i == indexY ? 1 : 0);
		int skipX = 0;
		for (int j = 0; j < X - 1; ++j)
		{
			skipX += (j == indexX ? 1 : 0);
			ret[i][j] = this->columns[i + skipY][j + skipX];
		}
	}
	return ret;
}

template<typename T, unsigned int X, unsigned int Y>
T Mat<T, X, Y>::GetTrace()
{
	T ret = 0;
	unsigned m = min(X, Y);
	for (unsigned i = 0; i < m; i++)
	{
		ret += this->columns[i][i];
	}
	return ret;
}

template<typename T, unsigned int X, unsigned int Y>
Mat<T, X, Y> Mat<T, X, Y>::Min(const Mat<T, X, Y> & a, const Mat<T, X, Y> & b)
{
	Mat<T, X, Y> ret;
	for (unsigned i = 0; i < X * Y; i++)
	{
		ret.elements[i] = min(a.elements[i], b.elements[i]);
	}
	return ret;
}

template<typename T, unsigned int X, unsigned int Y>
Mat<T, X, Y> Mat<T, X, Y>::Max(const Mat<T, X, Y> & a, const Mat<T, X, Y> & b)
{
	Mat<T, X, Y> ret;
	for (unsigned i = 0; i < X * Y; i++)
	{
		ret.elements[i] = max(a.elements[i], b.elements[i]);
	}
	return ret;
}

template<typename T, unsigned int X, unsigned int Y>
Mat<T, X, Y> Mat<T, X, Y>::CompMult(const Mat<T, X, Y> & a, const Mat<T, X, Y> & b)
{
	Mat<T, X, Y> ret;
	for (unsigned i = 0; i < X * Y; i++)
	{
		ret.elements[i] = a.elements[i] * b.elements[i];
	}
	return ret;
}

template<typename T, unsigned int X, unsigned int Y>
string Mat<T, X, Y>::ToString() const
{
	string ret;
	for (unsigned i = 0; i < X; ++i)
	{
		for (unsigned j = 0; j < Y; ++j)
		{
			string s(to_string(this->columns[j][i]).substr(0, 8));
			ret += "[" + s + "]";
		}
		ret += '\n';
	}
	ret.pop_back();
	return ret;
}

template<typename T, unsigned X, unsigned Y>
template<unsigned X2, unsigned Y2, typename>
auto & Mat<T, X, Y>::Transpose()
{
	for (unsigned i = 0; i < Y; ++i)
	{
		for (unsigned j = i + 1; j < X; ++j)
		{
			T temp = this->columns[i][j];
			this->columns[i][j] = this->columns[j][i];
			this->columns[j][i] = temp;
		}
	}
	return *this;
}

template<typename T, unsigned int X, unsigned int Y>
template<unsigned, unsigned, typename>
T Mat<T, X, Y>::GetDeterminante(int)
{
	T ret = T();
	for (unsigned i = 0; i < X; ++i)
	{
		T sign = i % 2 == 0 ? 1.0 : -1.0;
		ret += this->columns[i][0] * this->GetMinor(0, i).GetDeterminante() * sign;
	}
	return ret;
}

template<typename T, unsigned int X, unsigned int Y>
template<unsigned, unsigned, typename>
Mat<T, X, X> & Mat<T, X, Y>::Invert()
{
	Mat<T, X, X> inv;
	T det = T();//= this->GetDeterminante();

	for (unsigned i = 0; i < X; ++i)
	{
		for (unsigned j = 0; j < X; ++j)
		{
			T sign = i % 2 == j % 2 ? 1.0 : -1.0;
			inv[i][j] = this->GetMinor(j, i).GetDeterminante() * sign;
		}
	}
	inv.Transpose();
	for (int i = 0; i < X; ++i)
	{
		det += this->columns[0][i] * inv.columns[i][0];
	}
	ASSERT(det != 0); //ASSERT(det >= 1e-7 || det <= -1e-7);
	*this = inv * (1 / det);
	return *this;
}

template<typename T, unsigned int X, unsigned int Y>
template<unsigned X2, unsigned Y2, typename>
Mat<T, X, X> & Mat<T, X, Y>::Invert(int)
{
	T * m = this->elements.data();
	T inv[16];
	T det;

	inv[0] = m[5] * m[10] * m[15] - m[5] * m[11] * m[14] - m[9] * m[6] * m[15] + m[9] * m[7] * m[14] + m[13] * m[6] * m[11] - m[13] * m[7] * m[10];
	inv[4] = -m[4] * m[10] * m[15] + m[4] * m[11] * m[14] + m[8] * m[6] * m[15] - m[8] * m[7] * m[14] - m[12] * m[6] * m[11] + m[12] * m[7] * m[10];
	inv[8] = m[4] * m[9] * m[15] - m[4] * m[11] * m[13] - m[8] * m[5] * m[15] + m[8] * m[7] * m[13] + m[12] * m[5] * m[11] - m[12] * m[7] * m[9];
	inv[12] = -m[4] * m[9] * m[14] + m[4] * m[10] * m[13] + m[8] * m[5] * m[14] - m[8] * m[6] * m[13] - m[12] * m[5] * m[10] + m[12] * m[6] * m[9];
	inv[1] = -m[1] * m[10] * m[15] + m[1] * m[11] * m[14] + m[9] * m[2] * m[15] - m[9] * m[3] * m[14] - m[13] * m[2] * m[11] + m[13] * m[3] * m[10];
	inv[5] = m[0] * m[10] * m[15] - m[0] * m[11] * m[14] - m[8] * m[2] * m[15] + m[8] * m[3] * m[14] + m[12] * m[2] * m[11] - m[12] * m[3] * m[10];
	inv[9] = -m[0] * m[9] * m[15] + m[0] * m[11] * m[13] + m[8] * m[1] * m[15] - m[8] * m[3] * m[13] - m[12] * m[1] * m[11] + m[12] * m[3] * m[9];
	inv[13] = m[0] * m[9] * m[14] - m[0] * m[10] * m[13] - m[8] * m[1] * m[14] + m[8] * m[2] * m[13] + m[12] * m[1] * m[10] - m[12] * m[2] * m[9];
	inv[2] = m[1] * m[6] * m[15] - m[1] * m[7] * m[14] - m[5] * m[2] * m[15] + m[5] * m[3] * m[14] + m[13] * m[2] * m[7] - m[13] * m[3] * m[6];
	inv[6] = -m[0] * m[6] * m[15] + m[0] * m[7] * m[14] + m[4] * m[2] * m[15] - m[4] * m[3] * m[14] - m[12] * m[2] * m[7] + m[12] * m[3] * m[6];
	inv[10] = m[0] * m[5] * m[15] - m[0] * m[7] * m[13] - m[4] * m[1] * m[15] + m[4] * m[3] * m[13] + m[12] * m[1] * m[7] - m[12] * m[3] * m[5];
	inv[14] = -m[0] * m[5] * m[14] + m[0] * m[6] * m[13] + m[4] * m[1] * m[14] - m[4] * m[2] * m[13] - m[12] * m[1] * m[6] + m[12] * m[2] * m[5];
	inv[3] = -m[1] * m[6] * m[11] + m[1] * m[7] * m[10] + m[5] * m[2] * m[11] - m[5] * m[3] * m[10] - m[9] * m[2] * m[7] + m[9] * m[3] * m[6];
	inv[7] = m[0] * m[6] * m[11] - m[0] * m[7] * m[10] - m[4] * m[2] * m[11] + m[4] * m[3] * m[10] + m[8] * m[2] * m[7] - m[8] * m[3] * m[6];
	inv[11] = -m[0] * m[5] * m[11] + m[0] * m[7] * m[9] + m[4] * m[1] * m[11] - m[4] * m[3] * m[9] - m[8] * m[1] * m[7] + m[8] * m[3] * m[5];
	inv[15] = m[0] * m[5] * m[10] - m[0] * m[6] * m[9] - m[4] * m[1] * m[10] + m[4] * m[2] * m[9] + m[8] * m[1] * m[6] - m[8] * m[2] * m[5];

	det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];
	assert(det != 0);
	det = 1.0f / det;
	for (int i = 0; i < 16; i++) { m[i] = inv[i] * det; }
	return *this;
}

template<typename T, unsigned int X, unsigned int Y>
template<unsigned X2, unsigned Y2, typename>
T Mat<T, X, Y>::LenghtSquared() const
{
	T pows = T();
	for (unsigned i = 0; i < X; i++)
	{
		pows += pow(this->elements[i], 2);
	}
	return pows;
}

template<typename T, unsigned int X, unsigned int Y>
template<unsigned X2, unsigned Y2, typename>
Mat<T, X, Y> Mat<T, X, Y>::Cross(Mat<T, X, Y> const & other) const
{
	Mat<T, X, Y> ret;
	ret.x = this->y * other.z - this->z * other.y;
	ret.y = this->z * other.x - this->x * other.z;
	ret.z = this->x * other.y - this->y * other.x;
	return ret;
}
}
}
