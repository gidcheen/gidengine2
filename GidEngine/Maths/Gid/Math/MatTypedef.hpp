#pragma once

namespace Gid
{
namespace Maths
{
template<typename T, unsigned X, unsigned Y>
struct Mat;

// mat
template<unsigned X, unsigned Y>
using Matf = Mat<float, X, Y>;
template<unsigned X, unsigned Y>
using Matd = Mat<double, X, Y>;
template<unsigned X, unsigned Y>
using Mati = Mat<int, X, Y>;

using Mat2f = Mat<float, 2, 2>;
using Mat2d = Mat<double, 2, 2>;
using Mat2i = Mat<int, 2, 2>;

using Mat3f = Mat<float, 3, 3>;
using Mat3d = Mat<double, 3, 3>;
using Mat3i = Mat<int, 3, 3>;

using Mat4f = Mat<float, 4, 4>;
using Mat4d = Mat<double, 4, 4>;
using Mat4i = Mat<int, 4, 4>;

// vec
template<typename T, unsigned S>
using Vec = Mat<T, S, 1>;
template<unsigned S>
using Vecf = Mat<float, S, 1>;
template<unsigned S>
using Vecd = Mat<double, S, 1>;
template<unsigned S>
using Veci = Mat<int, S, 1>;

//using Vec2f = Vec<float, 2>;
using Vec2f = Vec<float, 2>;
using Vec2d = Vec<double, 2>;
using Vec2i = Vec<int, 2>;
using Vec2b = Vec<bool, 2>;

//using Vec3f = Vec<float, 3>;
using Vec3f = Vec<float, 3>;
using Vec3d = Vec<double, 3>;
using Vec3i = Vec<int, 3>;

//using Vec4 = Vec<float, 4>;
using Vec4f = Vec<float, 4>;
using Vec4d = Vec<double, 4>;
using Vec4i = Vec<int, 4>;
}
}