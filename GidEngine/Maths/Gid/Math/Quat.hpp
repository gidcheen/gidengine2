#pragma once

#include <type_traits>
#include <string>

using namespace std;
namespace Gid
{
namespace Maths
{
template<typename T, unsigned X, unsigned Y>
struct Mat;

template<typename T>
struct Quat
{
	static_assert(is_floating_point_v<T>, "Only floating point allowed");

	union
	{
		struct
		{
			T x;
			T y;
			T z;
			T w;
		};
		Mat<T, 3, 1> v;
		Mat<T, 4, 1> values;
	};

	Quat();
	Quat(Mat<T, 3, 1> const & v, T const & w) : v(v), w(w) {}
	explicit Quat(Mat<T, 4, 1> const & values) : values(values) {}

	Quat<T> & operator+=(Quat<T> const & other);
	Quat<T> & operator-=(Quat<T> const & other) { return this += other * -1; }
	Quat<T> operator+(Quat<T> const & other) { return Quat<T>(*this) += other; }
	Quat<T> operator-(Quat<T> const & other) { return Quat<T>(*this) -= other; }

	Quat<T> & operator*=(T const & other);
	Quat<T> & operator/=(T const & other) { return *this *= 1 / other; }
	Quat<T> operator*(T const & other) const { return Quat<T>(*this) *= other; }
	Quat<T> operator/(T const & other) const { return Quat<T>(*this) /= other; }

	Quat<T> & operator*=(Quat<T> const & other) { return *this = *this * other; }
	Quat<T> & operator/=(Quat<T> const & other) { return *this = *this / other; }
	Quat<T> operator*(Quat<T> const & other) const;
	Quat<T> operator/(Quat<T> const & other) const { return *this * other.Inverted(); }

	Mat<T, 3, 1> operator*(Mat<T, 3, 1> const & other) const { return (*this * Quat<T>(other, 0) * this->Inverted()).v; }

	Quat<T> & Invert();
	Quat<T> & Inverted() const { return Quat(*this).Invert(); }

	T LenghtSquared() const;
	T Length() const { return sqrt(LenghtSquared()); }

	Quat<T> & Normalize();
	Quat<T> Normalized() { return Quat<T>(*this).Normalize(); }

	static Quat<T> FromAxisAngle(Mat<T, 3, 1> const & n, T const & a) { return Quat<T>((n.Normalized() * sin(a / 2)), cos(a / 2)).Normalize(); }

	string ToString() const;
};

using Quatf = Quat<float>;
using Quatd = Quat<double>;
}
}

#include "Quat.impl.hpp"
