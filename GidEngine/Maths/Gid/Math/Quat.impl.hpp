#pragma once

#include "Quat.hpp"
#include "Math.hpp"
#include "Mat.hpp"

namespace Gid
{
namespace Maths
{
template<typename T>
Quat<T>::Quat() : Quat({ 0, 0, 0, 0 }) {}

template<typename T>
Quat<T> & Quat<T>::operator+=(const Quat<T> & other)
{
	w += other.w;
	v += other.v;
	return *this;
}

template<typename T>
Quat<T> & Quat<T>::operator*=(const T & other)
{
	w *= other;
	v *= other;
	return *this;
}

template<typename T>
Quat<T> Quat<T>::operator*(Quat const & other) const
{
	Quat<T> ret;
	ret.w = w * other.w - v.Dot(other.v);
	ret.v = v * other.w + other.v * w + v.Cross(other.v);
	return ret;
}

template<typename T>
Quat<T> & Quat<T>::Invert()
{
	v -= v;
	return *this;
}

template<typename T>
inline T Quat<T>::LenghtSquared() const { return values.LenghtSquared(); }

template<typename T>
Quat<T> & Quat<T>::Normalize()
{
	values *= 1 / Length();
	return *this;
}

template<typename T>
string Quat<T>::ToString() const
{
	return values.ToString();
}
}
}
