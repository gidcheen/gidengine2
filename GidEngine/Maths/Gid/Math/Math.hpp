#pragma once

#include <cmath>

namespace Gid
{
namespace Maths
{
template<typename T, unsigned X, unsigned Y>
struct Mat;

template<typename T>
struct Quat;

class Math
{
public:
	static constexpr double PI = 3.141592653589793;

	template<typename T>
	static constexpr T ToRadians(T const & degrees) { return (T)(degrees * (PI / 180.0)); }
	template<typename T>
	static constexpr T ToEuler(T const & radians) { return (T)((radians * 180.0) / PI); }

	template<typename T>
	static Mat<T, 4, 4> Orthographic(T left, T right, T bottom, T top, T near, T far);
	template<typename T>
	static Mat<T, 4, 4> Perspective(T fov, T apsect, T near, T far);

	template<typename T>
	static Mat<T, 4, 4> Translation(Mat<T, 3, 1> const & translation);
	template<typename T>
	static Mat<T, 4, 4> Rotation(Quat<T> const & quat);
	template<typename T>
	static Mat<T, 4, 4> Scale(Mat<T, 3, 1> const & scale);
	template<typename T>
	static Mat<T, 4, 4> Transform(Mat<T, 3, 1> const & position, Quat<T> const & rotation, Mat<T, 3, 1> const & scale);

	template<typename T>
	static Mat<T, 3, 1> MatToPosition(Mat<T, 4, 4> const & mat);
	template<typename T>
	static Quat<T> MatToRotation(Mat<T, 4, 4> const & mat);
	template<typename T>
	static Mat<T, 3, 1> MatToScale(Mat<T, 4, 4> const & mat);

	template<typename T>
	Mat<T, 3, 1> QuatToEuler(Quat<T> const & quat);
	template<typename T>
	Quat<T> EulerToQuat(Mat<T, 3, 1> const & vec);
};
}
}

#include "Math.impl.hpp"
