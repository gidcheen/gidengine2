#include "AssetManager.hpp"

#include <fstream>

using namespace Gid;
using namespace Assets;

void AssetManager::ReloadAssetUIds()
{
	assetPaths.clear();
	for (auto const & p : fs::recursive_directory_iterator(assetDir))
	{
		if (!fs::is_regular_file(p)) { continue; }
		if (p.path().string().find(metaFileExtension) != string::npos) { continue; }
		fs::path metaFilePath = p.path().string() + metaFileExtension;

		if (fs::exists(p.path().string() + metaFileExtension))
		{
			JsonV uIdJson;
			auto iStream = ifstream(metaFilePath);
			iStream >> uIdJson;
			assetPaths[UId(uIdJson)] = p.path();
		}
		else
		{
			UId uId = UId::Next();
			assetPaths[uId] = p.path();
			auto oStream = ofstream(metaFilePath);
			oStream << uId.ToJson();
		}
	}
}
