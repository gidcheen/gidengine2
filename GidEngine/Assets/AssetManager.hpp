#pragma once

#include <map>
#include <memory>
#include <string>
#include <filesystem>

#include "Utils/UId.hpp"
#include "Utils/TypeInfo.hpp"
#include "Utils/Name.hpp"

using namespace std;
namespace fs = filesystem;

namespace Gid
{
namespace Assets
{
struct AssetBase;
template<typename T>
struct Asset;

template<typename T>
struct AssetLoader;

class AssetManager
{
private:
	string const assetDir = "../../Assets";
	string const metaFileExtension = ".meta";

	map<UId, map<Name, weak_ptr<AssetBase>>> loadedAssets;
	map<UId, fs::path> assetPaths;

public:
	AssetManager() { ReloadAssetUIds(); }

	static shared_ptr<AssetManager> GetInstance()
	{
		static shared_ptr<AssetManager> assetManager;
		if (!assetManager) { assetManager = make_shared<AssetManager>(); }
		return assetManager;
	}

	template<typename T, typename TLoader = AssetLoader<T>>
	shared_ptr<Asset<T>> GetAsset(UId const & uId);

	template<typename T, typename TLoader = AssetLoader<T>>
	shared_ptr<Asset<T>> GetAsset(string const & path);

	template<typename T, typename TLoader = AssetLoader<T>>
	bool IsAssetLoaded(UId const & uId) { return GetLoadedAsset<T, TLoader>(uId); }

	void ReloadAssetUIds();

private:
	template<typename T, typename TLoader = AssetLoader<T>>
	shared_ptr<Asset<T>> GetLoadedAsset(UId const & uId) { return dynamic_pointer_cast<Asset<T>>(loadedAssets[uId][TypeName<TLoader>()].lock()); }
	template<typename T, typename TLoader = AssetLoader<T>>
	shared_ptr<Asset<T>> LoadAsset(UId const & uId);
};
}
}

#include "AssetManager.impl.hpp"
