#pragma once

#include "AssetManager.hpp"

#include <fstream>

#include "MainLoop.hpp"
#include "Asset.hpp"
#include "Utils/Logging.hpp"
#include <thread>

namespace Gid
{
namespace Assets
{
template<typename T, typename TLoader>
inline shared_ptr<Asset<T>> AssetManager::GetAsset(UId const & uId)
{
	auto asset = GetLoadedAsset<T, TLoader>(uId);
	return asset ? asset : LoadAsset<T, TLoader>(uId);
}

template<typename T, typename TLoader>
inline shared_ptr<Asset<T>> AssetManager::LoadAsset(UId const & uId)
{
	auto path = assetPaths[uId];
	if (!fs::exists(path))
	{
		LOG_WARNING("Trying to load asset with path that doesnt exist: " + path.string());
		return nullptr;
	}

	auto fileEnding = StringUtils::GetFileEnding(path.string());
	TLoader loader;
	if (!loader.IsEndingValid(fileEnding))
	{
		LOG_ERROR("loader cant load file with ending: " + fileEnding);
		return nullptr;
	}

	auto asset = make_shared<Asset<T>>();
	auto loadFunc = [asset, loader, path]()
	{
		auto loaded = loader.Load(path);
		if (!loaded)
		{
			LOG_ERROR("Loader returned null asset");
			return;
		}
		asset->asset = loaded;
		asset->isLoading = false;
	};
	thread{ loadFunc }.detach();

	loadedAssets[uId][TypeName<TLoader>()] = asset;
	return asset;
}

template<typename T, typename TLoader>
inline shared_ptr<Asset<T>> AssetManager::GetAsset(string const & path)
{
	auto actualPath = assetDir + "/" + path;
	auto item = find_if(assetPaths.begin(), assetPaths.end(), [&actualPath](auto const & p) { return p.second == actualPath; });
	if (item == assetPaths.end())
	{
		LOG_WARNING("Trying to load asset with path that doesnt exist: " + path);
		return nullptr;
	}
	return GetAsset < T, TLoader > (item->first);
}
}
}
