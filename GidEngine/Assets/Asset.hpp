#pragma once

#include <memory>

#include "Utils/UId.hpp"

using namespace std;

namespace Gid
{
namespace Assets
{
struct AssetBase
{
	virtual ~AssetBase() = default;
};

template<typename T>
struct Asset : public AssetBase
{
	bool isLoading = true;
	shared_ptr<T> asset;

	Asset() = default;
	explicit Asset(shared_ptr<T> const & asset) : asset(asset) {}
	template<typename... Args>
	explicit Asset(Args const & ... args) : Asset(make_shared<T>(args...)) {}

	~Asset() override = default;

	auto Get() { return isLoading ? nullptr : asset; }

	T & operator*() { return *Get(); }
	T const & operator*() const { return *Get(); }

	T & operator->() { return *Get(); }
	T const & operator->() const { return *Get(); }
};
}
}
