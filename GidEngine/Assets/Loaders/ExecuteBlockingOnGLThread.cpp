#include "ExecuteBlockingOnGLThread.hpp"

#include <thread>
#include <mutex>
#include <condition_variable>

#include "GL/GL.hpp"
#include "Utils/PerThreadEventQueues.hpp"

using namespace Gid;

void Gid::Assets::ExecuteBlockingOnGLThread(function<void()> const & func)
{
	if (this_thread::get_id() == GL::GL::GetGLThreadId())
	{
		func();
	}
	else
	{
		mutex m;
		unique_lock lock(m);
		condition_variable condition;

		PerThreadEventQueues::Get(GL::GL::GetGLThreadId()).Enqueue(
			[&func, &condition]()
			{
				func();
				condition.notify_one();
			});
		condition.wait(lock);
	}
}
