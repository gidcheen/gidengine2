#pragma once

#include <algorithm>

#include "Assets/AssetLoader.hpp"
#include "GL/GLTexture.hpp"

using namespace Gid::GL;

namespace Gid
{
namespace Assets
{
template<typename T>
struct Asset;

template<>
struct AssetLoader<GLTexture>
{
	vector<string> const endings = { "png", "jpg", "jpeg", "psd", "tif" };

	bool IsEndingValid(string const & ending) const { return find(endings.begin(), endings.end(), ending) != endings.end(); }
	shared_ptr<GLTexture> Load(fs::path path) const;
};
}
}
