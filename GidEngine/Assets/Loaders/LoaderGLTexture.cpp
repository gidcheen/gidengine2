#include "LoaderGLTexture.hpp"

#include <fstream>

#define STB_IMAGE_IMPLEMENTATION
#include "_External/stb/stb_image.h"

#include "Assets/Asset.hpp"
#include "GL/GLTexture.hpp"
#include "GL/GL.hpp"
#include "Utils/Logging.hpp"
#include "ExecuteBlockingOnGLThread.hpp"

using namespace Gid;
using namespace Assets;
using namespace GL;
using namespace Maths;

shared_ptr<GLTexture> AssetLoader<GLTexture>::Load(fs::path path) const
{
	vector<char> fileContent(fs::file_size(path));
	auto iFile = ifstream(path, ios::binary);
	fileContent.assign(istreambuf_iterator(iFile), istreambuf_iterator<char>());

	int width;
	int height;
	int nrChannels;
	auto * stbiData = stbi_load_from_memory(reinterpret_cast<unsigned char*>(fileContent.data()), static_cast<int>(fileContent.size()), &width, &height, &nrChannels, 0);

	if (stbiData == nullptr)
	{
		LOG("image could not be loaded");
		return nullptr;
	}

	auto size = width * height * nrChannels;
	vector<char> data(static_cast<unsigned long>(size));
	data.assign(stbiData, stbiData + size);
	stbi_image_free(stbiData);

	shared_ptr<GLTexture> ret;

	ExecuteBlockingOnGLThread([&]() { ret = make_shared<GLTexture>(data, Vec2i{ width, height }); });
	return ret;
}
