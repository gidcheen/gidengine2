#pragma once

#include <algorithm>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Assets/AssetLoader.hpp"
#include "Graphics/Model.hpp"

using namespace Gid::Graphics;

namespace Gid
{
namespace Graphics
{
class MeshData;
}
namespace Assets
{
template<typename T>
struct Asset;

template<>
struct AssetLoader<Model>
{
	vector<string> const endings = { "blend", "dae" };

	bool IsEndingValid(string const & ending) const { return find(endings.begin(), endings.end(), ending) != endings.end(); }
	shared_ptr<Model> Load(fs::path path) const;

private:
	void AddMesh(vector<shared_ptr<MeshData>> & meshes, aiScene const * scene, aiNode const * node, Mat4f const & parentTransform) const;
	shared_ptr<MeshData> CreateMesh(aiMesh const * mesh, Mat4f const & transform) const;

	Mat4f AiMatToMat4(const aiMatrix4x4 src) const { return Mat4f(&src.a1).Transpose(); }
};
}
}
