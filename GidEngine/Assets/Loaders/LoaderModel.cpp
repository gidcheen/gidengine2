#include "LoaderModel.hpp"

#include <fstream>

#include "Assets/Asset.hpp"
#include "ExecuteBlockingOnGLThread.hpp"
#include "Gid/Math/Maths.hpp"
#include "Utils/Logging.hpp"

using namespace Gid;
using namespace Assets;
using namespace Graphics;
using namespace Assimp;

shared_ptr<Model> AssetLoader<Model>::Load(fs::path path) const
{
	Importer importer;
	const aiScene * scene = importer.ReadFile(
		path.string(),
		aiProcess_Triangulate | aiProcess_CalcTangentSpace |
		aiProcess_JoinIdenticalVertices | aiProcess_FlipUVs);

	if (scene == nullptr)
	{
		LOG_ERROR("assimp failed could not import file: " + path.string() + "\nWith error: " + importer.GetErrorString());
		return nullptr;
	}

	vector<shared_ptr<MeshData>> mesheDatas;
	AddMesh(mesheDatas, scene, scene->mRootNode, Mat4f::Identity());

	shared_ptr<Model> ret;
	ExecuteBlockingOnGLThread(
		[&ret, &mesheDatas]()
		{
			vector<shared_ptr<Mesh>> meshes;
			auto f = [](shared_ptr<MeshData> const & meshData) { return make_shared<Mesh>(*meshData); };
			transform(begin(mesheDatas), end(mesheDatas), back_inserter(meshes), f);
			ret = make_shared<Model>(meshes);
		});
	return ret;
}

void AssetLoader<Model>::AddMesh(vector<shared_ptr<MeshData>> & meshes, aiScene const * scene, aiNode const * node, Mat4f const & parentTransform) const
{
	Mat4f transform = parentTransform * AiMatToMat4(node->mTransformation);

	for (unsigned i = 0; i < node->mNumMeshes; i++)
	{
		meshes.push_back(CreateMesh(scene->mMeshes[node->mMeshes[i]], transform));
	}

	for (unsigned i = 0; i < node->mNumChildren; i++)
	{
		AddMesh(meshes, scene, node->mChildren[i], transform);
	}
}

shared_ptr<MeshData> AssetLoader<Model>::CreateMesh(aiMesh const * mesh, Mat4f const & transform) const
{
	// layout info
	vector<MeshData::LayoutInfo> layouts;
	layouts.emplace_back(0, 3);
	layouts.emplace_back(1, 3);
	if (mesh->mTangents && mesh->mBitangents)
	{
		layouts.emplace_back(2, 3);
		layouts.emplace_back(3, 3);
	}
	if (mesh->mColors[0]) { layouts.emplace_back(4, 3); }
	if (mesh->mTextureCoords[0]) { layouts.emplace_back(5, 2); }

	// indices
	vector<Vec<GLuint, 3>> indices;
	for (unsigned i = 0; i < mesh->mNumFaces; i++)
	{
		auto aiIndices = mesh->mFaces[i].mIndices;
		indices.emplace_back(aiIndices[0], aiIndices[1], aiIndices[2]);
	}

	// vertices
	vector<float> vertices;
	BoundingBox bounds;
	for (unsigned i = 0; i < mesh->mNumVertices; i++)
	{
		Vec4f position(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z, 4);
		position = transform * position;
		vertices.insert(end(vertices), begin(position.elements), end(position.elements) - 1);

		Mat4f normalMat = transform.Inverse().Transposed();
		Vec4f normal(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z, 0);
		normal = normalMat * normal;
		vertices.insert(end(vertices), begin(normal.elements), end(normal.elements) - 1);

		if (mesh->mTangents && mesh->mBitangents)
		{
			Vec4f tangent(mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z, 0);
			tangent = normalMat * tangent;
			vertices.insert(end(vertices), begin(tangent.elements), end(tangent.elements) - 1);

			Vec4f bitangent(mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z, 0);
			bitangent = normalMat * bitangent;
			vertices.insert(end(vertices), begin(bitangent.elements), end(bitangent.elements) - 1);
		}

		if (mesh->mColors[0])
		{
			Vec4f color(mesh->mColors[0][i].r, mesh->mColors[0][i].g, mesh->mColors[0][i].b, mesh->mColors[0][i].a);
			vertices.insert(end(vertices), begin(color.elements), end(color.elements) - 1);
		}

		if (mesh->mTextureCoords[0])
		{
			Vec2f uv(mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y);
			vertices.insert(end(vertices), begin(uv.elements), end(uv.elements));
		}
	}

	auto data = make_shared<MeshData>(layouts);
	data->indices = indices;
	data->SetVertices(vertices);
	return data;
}
