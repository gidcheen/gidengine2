#pragma once

#include "functional"

using namespace std;

namespace Gid
{
namespace Assets
{
void ExecuteBlockingOnGLThread(function<void()> const & func);
}
}
