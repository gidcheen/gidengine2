#include "MainLoop.hpp"

#include <cassert>
#include <condition_variable>

using namespace Gid;

int MainLoop::Run(function<bool()> const & mainFunc)
{
	bool shouldRun = true;
	while (shouldRun)
	{
		shouldRun = mainFunc();
	}

	return 0;
}
