#pragma once

#include <map>
#include <vector>
#include <functional>
#include <mutex>
#include <thread>

using namespace std;

namespace Gid
{
struct MainLoop
{
	static int Run(function<bool()> const & mainFunc);
};
}
