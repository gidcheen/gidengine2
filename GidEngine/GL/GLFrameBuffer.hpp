#pragma once

#include <map>

#include "GLObject.hpp"
#include "GLTexture.hpp"
#include "Gid/Math/Mat.hpp"

namespace Gid
{
namespace GL
{
class GLFrameBuffer : public GLObject
{
private:
	const GLTexture * depthTexture = nullptr;
	const GLTexture * stencilTexture = nullptr;
	map<GLenum, const GLTexture *> colorTextures;

public:
	GLFrameBuffer() : GLObject([](GLuint * id) { glGenFramebuffers(1, id); })
	{
		Bind();
		Unbind();
		glNamedFramebufferDrawBuffer(id, GL_NONE);
		glNamedFramebufferReadBuffer(id, GL_NONE);
	}
	~GLFrameBuffer() override { glDeleteFramebuffers(1, &id); }

	void BindRead() const { glBindFramebuffer(GL_READ_FRAMEBUFFER, id); }
	void UnbindRead() const { glBindFramebuffer(GL_READ_FRAMEBUFFER, 0); }
	void BindWrite() const { glBindFramebuffer(GL_DRAW_FRAMEBUFFER, id); }
	void UnbindWrite() const { glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0); }

	void Clear() const
	{
		Bind(); // todo: use named function
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		Unbind();
	}

	const GLTexture * GetColorTexture(GLenum index)
	{
		return colorTextures.find(index) != colorTextures.end() ?
			   colorTextures[index] :
			   nullptr;
	}
	void SetColorTexture(const GLTexture * texture, GLuint index = GL_COLOR_ATTACHMENT0)
	{
		glNamedFramebufferTexture(id, GL_COLOR_ATTACHMENT0, texture ? texture->id : 0, 0);
		colorTextures[index] = texture;
	}

	const GLTexture * GetDepthTexture() const { return depthTexture; }
	void SetDepthTexture(const GLTexture * texture)
	{
		glNamedFramebufferTexture(id, GL_DEPTH_ATTACHMENT, texture ? texture->id : 0, 0);
		depthTexture = texture;
	}

	const GLTexture * GetStencilTexture() const { return stencilTexture; }
	void SetStencilTexture(const GLTexture * texture)
	{
		glNamedFramebufferTexture(id, GL_STENCIL_ATTACHMENT, texture ? texture->id : 0, 0);
		stencilTexture = texture;
	}

	void SetDepthStencilTexture(const GLTexture * texture)
	{
		glNamedFramebufferTexture(id, GL_DEPTH_STENCIL_ATTACHMENT, texture ? texture->id : 0, 0);
		depthTexture = texture;
		stencilTexture = texture;
	}

protected:
	void BindTo(GLuint id) const override { glBindFramebuffer(GL_FRAMEBUFFER, id); }
};
}
}
