#pragma once

#include <cassert>
#include <thread>
#include "_External/gl3w/gl3w.h"

using namespace std;

namespace Gid
{
namespace GL
{
class GL
{
private:
	inline static bool initialized = false;
	inline static thread::id glThreadId;

public:
	static auto const & GetGLThreadId() { return glThreadId; }

	static void Init();

	template<typename T>
	static GLenum GetType() { return 0; }

	static GLenum GetGLChannel(int channelCount)
	{
		switch (channelCount)
		{
		case 1:
			return GL_RED;
		case 2:
			return GL_RG;
		case 3:
			return GL_RGB;
		case 4:
			return GL_RGBA;
		default:
			assert(false);
			return 0;
		}
	}

private:
	static void ErrorCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar * message, const void * userParam);
};

template<>
inline GLenum GL::GetType<char>() { return GL_BYTE; }
template<>
inline GLenum GL::GetType<unsigned char>() { return GL_UNSIGNED_BYTE; }
template<>
inline GLenum GL::GetType<int>() { return GL_INT; }
template<>
inline GLenum GL::GetType<unsigned>() { return GL_UNSIGNED_INT; }
template<>
inline GLenum GL::GetType<float>() { return GL_FLOAT; }
}
}
