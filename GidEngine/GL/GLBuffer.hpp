#pragma once

#include <vector>

#include "GLObject.hpp"

using namespace std;

namespace Gid
{
namespace GL
{
template<typename T, GLenum Type>
class GLBuffer : public GLObject
{
private:
	static constexpr GLenum defaultUsage = GL_STATIC_DRAW;
	GLenum usage = 0;

public:
	explicit GLBuffer(GLenum usage = defaultUsage) : GLBuffer(vector<T>(), usage) {}
	explicit GLBuffer(vector<T> const & data, GLenum usage = defaultUsage) : GLBuffer(data.data(), data.size(), usage) {}
	GLBuffer(const T * data, size_t size, GLenum usage = defaultUsage) : GLObject([](GLuint * id) { glGenBuffers(1, id); }), usage(usage)
	{
		Bind();
		Unbind();
		Set(data, size);
	}
	~GLBuffer() override { glDeleteBuffers(1, &id); }

	GLint GetSize() const
	{
		GLint size;
		glGetNamedBufferParameteriv(id, GL_BUFFER_SIZE, &size);
		return size;
	}

	GLenum GetUsage() const { return usage; }

	static constexpr int GetElementSize() { return sizeof(T); }

	GLint GetCount() const
	{
		return GetSize() / GetElementSize();
	}

	void Set(vector<T> const & data) { Set(data.data(), data.size()); }
	void Set(T const * data, size_t size)
	{
		glNamedBufferData(id, size * GetElementSize(), data, usage);
	}

	void SetSub(vector<T> const & data, int offset = 0)
	{
		glNamedBufferSubData(id, offset * GetElementSize(), data.size() * GetElementSize(), data.data());
	}

protected:
	void BindTo(GLuint id) const override { glBindBuffer(Type, id); }
};

using GLIndexBuffer = GLBuffer<GLuint, GL_ELEMENT_ARRAY_BUFFER>;
using GLVertexBuffer = GLBuffer<GLfloat, GL_ARRAY_BUFFER>;
}
}
