#pragma once

#include <algorithm>

#include "GLObject.hpp"
#include "GLProgram.hpp"
#include "GLTexture.hpp"

namespace Gid
{
namespace GL
{
class GLProgramPipeline : public GLObject
{
private:
	vector<GLProgram const *> programs{};

public:
	GLProgramPipeline() : GLObject([](GLuint * id) { glGenProgramPipelines(1, id); }) {}
	~GLProgramPipeline() override { glDeleteProgramPipelines(1, &id); }

	void Add(const GLProgram * program)
	{
		glUseProgramStages(id, program->GetStageBit(), program->id);
		programs.push_back(program);
	}

	template<typename T>
	void SetUniform(const string & name, const T & value) const { for (auto * p : programs) { p->SetUniform(name, value); } }

protected:
	void BindTo(GLuint id) const override { glBindProgramPipeline(id); }
};
}
}
