#pragma once

#include <functional>

#include "GL.hpp"

using namespace std;

namespace Gid
{
namespace GL
{
class GLObject
{
public:
	GLuint const id;

protected:
	using CreateFunc = function<void(GLuint *)>;

public:
	explicit GLObject(CreateFunc const & createFunc) : id(CreateId(createFunc)) {}
	virtual ~GLObject() = default;

	bool operator==(GLObject const & other) const { return id == other.id; }
	bool operator!=(GLObject const & other) const { return id != other.id; }
	bool operator<(GLObject const & other) const { return id < other.id; }

	void Bind() const { BindTo(id); }
	void Unbind() const { BindTo(0); }

protected:
	virtual void BindTo(GLuint id) const = 0;

private:
	GLuint CreateId(CreateFunc const & createFunc) const
	{
		GLuint id;
		createFunc(&id);
		return id;
	}
};
}
}
