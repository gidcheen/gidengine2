#pragma once

#include <vector>

#include "GLTextureBase.hpp"

namespace Gid
{
namespace GL
{
class GLTexture : public GLTextureBase
{
public:
	static constexpr GLenum Target = GL_TEXTURE_2D;
	bool generateMips;

private:
	bool hasMips = false;

	GLenum wrapping = 0;
	GLenum filteringMin = 0;
	GLenum filteringMag = 0;

	GLenum format = 0;
	GLenum internalFormat = 0;
	GLenum type = 0;

public:
	template<typename T>
	GLTexture(
		T const & data,
		Vec2i size,
		bool generateMips = false,
		GLenum wrapping = GL_REPEAT,
		GLenum filteringMin = GL_LINEAR_MIPMAP_LINEAR,
		GLenum filteringMag = GL_LINEAR) :
		GLTextureBase(Target),
		generateMips(generateMips)
	{
		SetWrapping(wrapping);
		SetFilteringMin(filteringMin);
		SetFilteringMag(filteringMag);
		SetData(data, size);
	}
	~GLTexture() override = default;

	bool HasMips() const { return hasMips; }

	GLenum GetWrapping() const { return wrapping; }
	void SetWrapping(GLenum wrapping)
	{
		this->wrapping = wrapping;
		glTextureParameteri(id, GL_TEXTURE_WRAP_S, wrapping);
		glTextureParameteri(id, GL_TEXTURE_WRAP_T, wrapping);
		glTextureParameteri(id, GL_TEXTURE_WRAP_R, wrapping);
	}

	GLenum GetFilteringMin() const { return filteringMin; }
	void SetFilteringMin(GLenum filteringMin)
	{
		this->filteringMin = filteringMin;
		glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, filteringMin);
	}

	GLenum GetFilteringMag() const { return filteringMag; }
	void SetFilteringMag(GLenum filteringMag)
	{
		this->filteringMag = filteringMag;
		glTextureParameteri(id, GL_TEXTURE_MAG_FILTER, filteringMag);
	}

	GLenum GetInternalFormat() const { return internalFormat; }
	GLenum GetFormat() const { return format; }
	GLenum GetType() const { return type; }

	void GenerateMips()
	{
		hasMips = true;
		glGenerateTextureMipmap(id);
	}

	template<typename T>
	void SetData(const vector<T> & data, Vec2i const & size, GLenum target = Target)
	{
		auto channel = GL::GetGLChannel((int)data.size() / (size.x * size.y));
		SetData(data.data(), data.size(), size, channel, channel, target);
	}

	template<typename T>
	void SetData(const T * data, size_t length, Vec2i const & size, GLenum target = Target)
	{
		auto channel = GL::GetGLChannel((int)length / (size.x * size.y));
		SetData(data, length, size, channel, channel, target);
	}

	template<typename T>
	void SetData(const vector<T> & data, Vec2i const & size, GLenum internalFormat, GLenum format, GLenum target = Target)
	{
		SetData(data.data(), data.size(), size, internalFormat, format, target);
	}

	template<typename T>
	void SetData(const T * data, size_t lenght, Vec2i const & size, GLenum internalFormat, GLenum format, GLenum target = Target)
	{
		this->size = size;
		this->internalFormat = internalFormat;
		this->format = format;
		type = GL::GetType<T>();
		Bind();
		glTexImage2D(target, 0, internalFormat, size.x, size.y, 0, format, type, data);
		Unbind();
		if (generateMips) { GenerateMips(); }
	}
};
}
}