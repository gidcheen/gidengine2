#include "GL.hpp"

#include "Utils/Logging.hpp"

using namespace Gid;

void GL::GL::Init()
{
	if (initialized) { return; }
	auto initSuccess = gl3wInit() == GL3W_OK;
	assert(initSuccess);

	string const renderer((char *)glGetString(GL_RENDERER));
	string const version((char *)glGetString(GL_VERSION));
	LOG("Renderer: " + renderer + "\n" + "OpenGL version: " + version);

#ifndef NDEBUG
	GLint flags;
	glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	assert(flags & GL_CONTEXT_FLAG_DEBUG_BIT); // make sure debuging is available

	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(ErrorCallback, nullptr);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
#endif

	glThreadId = this_thread::get_id();

	initialized = true;
}

void GL::GL::ErrorCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar * message, const void * userParam)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204 || id == 131076) { return; }

	string log;
	log += "Debug message(" + to_string(id) + "): " + string(message) + "\n";

	switch (source)
	{
	case GL_DEBUG_SOURCE_API:
		log += "Source: API";
		break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
		log += "Source: Window System";
		break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		log += "Source: Shader Compiler";
		break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:
		log += "Source: Third Party";
		break;
	case GL_DEBUG_SOURCE_APPLICATION:
		log += "Source: Application";
		break;
	case GL_DEBUG_SOURCE_OTHER:
		log += "Source: Other";
		break;
	default:
		assert(false);
		break;
	}
	log += '\n';

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:
		log += "Type: Error";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		log += "Type: Deprecated Behaviour";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		log += "Type: Undefined Behaviour";
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		log += "Type: Portability";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		log += "Type: Performance";
		break;
	case GL_DEBUG_TYPE_MARKER:
		log += "Type: Marker";
		break;
	case GL_DEBUG_TYPE_PUSH_GROUP:
		log += "Type: Push Group";
		break;
	case GL_DEBUG_TYPE_POP_GROUP:
		log += "Type: Pop Group";
		break;
	case GL_DEBUG_TYPE_OTHER:
		log += "Type: Other";
		break;
	default:
		assert(false);
		break;
	}
	log += '\n';

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:
		log += "Severity: high";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		log += "Severity: medium";
		break;
	case GL_DEBUG_SEVERITY_LOW:
		log += "Severity: low";
		break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		log += "Severity: notification";
		break;
	default:
		assert(false);
		break;
	}

	if (severity == GL_DEBUG_SEVERITY_NOTIFICATION)
	{
		//LOG(log);
	}
	else
	{
		LOG_ERROR(log);
		assert(false);
	}
}
