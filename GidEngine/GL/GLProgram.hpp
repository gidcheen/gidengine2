#pragma once

#include <vector>
#include <string>
#include <cassert>

#include "GLObject.hpp"
#include "GLTexture.hpp"
#include "Utils/Logging.hpp"

using namespace std;

namespace Gid
{
namespace GL
{
class GLProgram : public GLObject
{
public:
	const GLenum type;

public:
	explicit GLProgram(GLenum type, vector<string> const & srcs) :
		GLObject([&](GLuint * id) { Create(id, srcs, type); }),
		type(type) {}
	~GLProgram() override { glDeleteProgram(id); }

	GLint GetUniformLocation(const string & name) const { return glGetUniformLocation(id, name.c_str()); }

	void SetUniform(const string & name, int value) const { SetUniform(GetUniformLocation(name), value); }
	void SetUniform(const string & name, float value) const { SetUniform(GetUniformLocation(name), value); }
	void SetUniform(const string & name, const Vec2f & value) const { SetUniform(GetUniformLocation(name), value); }
	void SetUniform(const string & name, const Vec3f & value) const { SetUniform(GetUniformLocation(name), value); }
	void SetUniform(const string & name, const Vec4f & value) const { SetUniform(GetUniformLocation(name), value); }
	void SetUniform(const string & name, const Mat4f & value) const { SetUniform(GetUniformLocation(name), value); }

	void SetUniform(GLint loc, int value) const { glProgramUniform1i(id, loc, value); }
	void SetUniform(GLint loc, float value) const { glProgramUniform1f(id, loc, value); }
	void SetUniform(GLint loc, const Vec2f & value) const { glProgramUniform2f(id, loc, value.x, value.y); }
	void SetUniform(GLint loc, const Vec3f & value) const { glProgramUniform3f(id, loc, value.x, value.y, value.z); }
	void SetUniform(GLint loc, const Vec4f & value) const { glProgramUniform4f(id, loc, value.x, value.y, value.z, value.w); }
	void SetUniform(GLint loc, const Mat4f & value) const { glProgramUniformMatrix4fv(id, loc, 1, GL_FALSE, &value[0][0]); }

protected:
	void BindTo(GLuint id) const override { glUseProgram(id); }

private:
	void Create(GLuint * id, const std::vector<string> & srcs, GLenum type)
	{
		vector<const char *> cstrs;
		for (const auto & s : srcs) { cstrs.push_back(s.c_str()); }
		*id = glCreateShaderProgramv(type, (GLsizei)cstrs.size(), cstrs.data());

		GLint validated;
		glGetProgramiv(*id, GL_LINK_STATUS, &validated);
		if (validated) { return; }

		GLint length;
		glGetProgramiv(*id, GL_INFO_LOG_LENGTH, &length);
		vector<GLchar> info((size_t)length);
		glGetProgramInfoLog(*id, length, &length, &info[0]);
		LOG(string(info.begin(), info.end()));
		glDeleteProgram(*id);
		assert(validated);
	}

public:
	GLenum GetStageBit() const
	{
		switch (type)
		{
		case GL_VERTEX_SHADER:
			return GL_VERTEX_SHADER_BIT;
		case GL_TESS_CONTROL_SHADER:
			return GL_TESS_CONTROL_SHADER_BIT;
		case GL_TESS_EVALUATION_SHADER:
			return GL_TESS_EVALUATION_SHADER_BIT;
		case GL_GEOMETRY_SHADER:
			return GL_GEOMETRY_SHADER_BIT;
		case GL_FRAGMENT_SHADER:
			return GL_FRAGMENT_SHADER_BIT;
		case GL_COMPUTE_SHADER:
			return GL_COMPUTE_SHADER_BIT;
		default:
			assert(false);
			return 0;
		}
	};
};
}
}
