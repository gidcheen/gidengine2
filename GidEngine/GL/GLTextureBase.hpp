#pragma once

#include "GLObject.hpp"
#include "Gid/Math/Maths.hpp"

using namespace Gid::Maths;

namespace Gid
{
namespace GL
{
class GLTextureBase : public GLObject
{
public:
	const GLenum target;

protected:
	Vec2i size;

public:
	explicit GLTextureBase(GLenum target) :
		GLObject([](GLuint * id) { glGenTextures(1, id); }),
		target(target)
	{
		Bind();
		Unbind();
	}
	~GLTextureBase() override { glDeleteTextures(1, &id); }

	Vec2i const & GetSize() const { return size; }
	GLenum GetTarget() const { return target; }

protected:
	void BindTo(GLuint id) const override { glBindTexture(this->target, id); }
};
}
}
