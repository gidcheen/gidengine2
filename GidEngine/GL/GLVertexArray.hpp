#pragma once

#include <memory>
#include <map>

#include "GLBuffer.hpp"

using namespace std;

namespace Gid
{
namespace GL
{
class GLVertexArray : public GLObject
{
private:
	map<GLuint, shared_ptr<GLVertexBuffer>> vertexBuffers;

public:
	GLVertexArray() : GLObject([](GLuint * id) { glGenVertexArrays(1, id); })
	{
		Bind();
		Unbind();
	}
	~GLVertexArray() override { glDeleteVertexArrays(1, &id); }

	void EnableVertexAttrib(shared_ptr<GLVertexBuffer> const & vertexBuffer, GLuint index, GLint componentCount, GLsizei stride, GLsizei offset)
	{
		vertexBuffers[index] = vertexBuffer;
		glEnableVertexArrayAttrib(id, index);
		Bind();
		vertexBuffer->Bind();
		glVertexAttribPointer(index, componentCount, GL_FLOAT, GL_FALSE, stride * sizeof(float), (void *)(offset * sizeof(float)));
		vertexBuffer->Unbind();
		Unbind();
	}

	void DisableVertexAttrib(GLuint index)
	{
		glDisableVertexArrayAttrib(id, index);
		vertexBuffers.erase(index);
	}

	auto const & GetVertexBuffers() const { return vertexBuffers; }

protected:
	void BindTo(GLuint id) const override { glBindVertexArray(id); }
};
}
}
