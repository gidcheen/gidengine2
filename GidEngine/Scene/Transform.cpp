#include "Transform.hpp"

#include <algorithm>

using namespace GidEngine;

void TransformBase::SetPosition(Vec3f const & position)
{
	this->position = position;
	SetDirty();
}
void TransformBase::SetRotation(Quatf const & rotation)
{
	this->rotation = rotation;
	SetDirty();
}

void TransformBase::SetScale(Vec3f const & scale)
{
	this->scale = scale;
	SetDirty();
}

Quatf TransformBase::GetGlobalRotation()
{
	return GetParent() ? Math::MatToRotation(GetParent()->GetLocalToWorldMatrix() * Math::Rotation(rotation)) : rotation;
}

void TransformBase::SetGlobalRotation(Quatf const & rotation)
{
	SetRotation(GetParent() ? Math::MatToRotation(GetParent()->GetWorldToLocalMatrix() * Math::Rotation(rotation)) : rotation);
}

Vec3f TransformBase::GetGlobalForward()
{
	Mat4f globalRot = GetParent()->GetLocalToWorldMatrix() * Math::Rotation(rotation);
	return Vec3f(globalRot.columns[2].data());
}

Vec3f TransformBase::GetGlobalRight()
{
	Mat4f globalRot = GetParent()->GetLocalToWorldMatrix() * Math::Rotation(rotation);
	return Vec3f(globalRot.columns[0].data());
}

Vec3f TransformBase::GetGlobalUp()
{
	Mat4f globalRot = GetParent()->GetLocalToWorldMatrix() * Math::Rotation(rotation);
	return Vec3f(globalRot.columns[1].data());
}

Mat4f TransformBase::GetLocalToWorldMatrix()
{
	if (isDirty)
	{
		isDirty = false;
		if (GetParent())
		{
			localToWorldMatrix = GetParent()->GetLocalToWorldMatrix() * CalculateLocalToParentMatrix();
		}
		else
		{
			localToWorldMatrix = CalculateLocalToParentMatrix();
		}
	}
	return localToWorldMatrix;
}

Mat4f TransformBase::GetWorldToLocalMatrix()
{
	if (isInverseDirty)
	{
		isInverseDirty = false;
		worldToLocalMatrix = GetLocalToWorldMatrix().Inverse();
	}
	return worldToLocalMatrix;
}

void TransformBase::SetDirty()
{
	isDirty = true;
	isInverseDirty = true;
	for (auto t : GetChildren())
	{
		t->SetDirty();
	}
}
