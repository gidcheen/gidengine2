#pragma once

#include <type_traits>

#include "Gid/Math/Maths.hpp"

namespace GidEngine
{
template<typename T>
struct Rect
{
	static_assert(is_fundamental_v<T>, "only basic types allowed");
	using VecT = Vec<T, 2>;

	VecT min;
	VecT max;

	Rect() = default;
	Rect(VecT const & pos, VecT const & size, ) : min(pos), max(min + size) {}

	VecT GetSize() const { return max - min; }
	void SetSize(VecT const & size) { max = min + size; }
	T GetWidth() const { return GetSize().x; }
	T GetHeight() const { return GetSize().y; }

	VecT GetCenter() const { return min + GetSize() * 0.5f; }
	void SetCenter(VecT const & newCenter)
	{
		auto delta = newCenter - GetCenter();
		min += delta;
		max += delta;
	}
};

using Rectf = Rect<float>;
using Rectd = Rect<double>;
using Recti = Rect<int>;
using Rectb = Rect<bool>;
}
