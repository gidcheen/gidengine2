#pragma once

#include <typeinfo>
#include "Parentable.hpp"
#include "Gid/Math/Maths.hpp"

using namespace Gid::Maths;

namespace GidEngine
{
class TransformBase : public Parentable<TransformBase>
{
private:
	Vec3f position;
	Quatf rotation;
	Vec3f scale;

	bool isDirty = true;
	bool isInverseDirty = true;
	Mat4f localToWorldMatrix;
	Mat4f worldToLocalMatrix;

public:
	TransformBase() = default;
	~TransformBase() override = default;

	// local
	Vec3f const & GetPosition() const { return position; }
	void SetPosition(const Vec3f & position);

	Quatf const & GetRotation() const { return rotation; }
	void SetRotation(const Quatf & rotation);

	Vec3f const & GetScale() const { return scale; }
	void SetScale(const Vec3f & scale);

	// global
	Vec3f GetGlobalPosition() { return Vec3f(GetLocalToWorldMatrix() * Vec4f(0, 0, 0, 1)); }
	void SetGlobalPosition(const Vec3f & position) { SetPosition(GetParent() ? Vec3f(GetParent()->GetWorldToLocalMatrix() * Vec4f(position, 1)) : position); }

	Quatf GetGlobalRotation();
	void SetGlobalRotation(Quatf const & rotation);

	void Translate(Vec3f const & delta) { SetPosition(GetPosition() + delta); }
	void Rotate(Quatf const & delta) { SetRotation(GetRotation() * delta); }
	void Scale(Vec3f const & delta) { SetScale(GetScale() + delta); }

	Vec3f GetForward() const { return Vec3f(Math::Rotation(rotation).columns[2].data()); }
	Vec3f GetRight() const { return Vec3f(Math::Rotation(rotation).columns[0].data()); }
	Vec3f GetUp() const { return Vec3f(Math::Rotation(rotation).columns[1].data()); }

	Vec3f GetGlobalForward();
	Vec3f GetGlobalRight();
	Vec3f GetGlobalUp();

	Mat4f GetLocalToWorldMatrix();
	Mat4f GetWorldToLocalMatrix();

	template<typename T>
	T * TryGetOwner() { return typeid(T) == GetOwnerType() ? static_cast<T *>(GetOwnerTypeless()) : nullptr; }

protected:
	void OnParentChanged() override { SetDirty(); }

	virtual type_info const & GetOwnerType() = 0;
	virtual void * GetOwnerTypeless() = 0;

private:
	Mat4f CalculateLocalToParentMatrix() { return Math::Transform(position, rotation, scale); }
	void SetDirty();
};

template<typename T>
class Transform : public TransformBase
{
private:
	T * owner;
public:
	explicit Transform(T * owner) : owner(owner) {}
	T * GetOwner() { return owner; }
protected:
	type_info const & GetOwnerType() override { return typeid(T); }
	void * GetOwnerTypeless() override { return owner; }
};
}
