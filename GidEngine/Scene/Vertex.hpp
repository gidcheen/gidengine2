#pragma once

#include "Gid/Math/Maths.hpp"

template<typename T, unsigned First, unsigned... Rest>
struct VertexImpl : public VertexImpl<T, Rest...>
{
	VertexImpl() : VertexImpl<T, Rest...>(), element(Vec<T, First>()) {}
	explicit VertexImpl(Vec<T, First> const & first, Vec<T, Rest> const & ... rest) : VertexImpl<T, Rest...>(rest...), element(first) {}
	Vec<T, First> element;
};

template<typename T, unsigned First>
struct VertexImpl<T, First>
{
	VertexImpl() : last(Vec<T, First>()) {}
	explicit VertexImpl(Vec<T, First> const & first) : last(first) {}
	Vec<T, First> last;
};


template<typename T, unsigned... Values>
class Vertex : public VertexImpl<T, Values...>
{
public:
	template<typename... Args>
	explicit Vertex(Args const & ... args) : VertexImpl<T, Values...>(args...) {}

	//static constexpr unsigned GetSize() { return sizeof...(Values); }

	static constexpr unsigned GetElementCount()
	{
		unsigned ret = 0;
		for (unsigned s : { Values... }) { ret += s; }
		return ret;
	}

private:
	template<unsigned Index, unsigned First2, unsigned... Rest2>
	static auto GetImpl(VertexImpl<T, First2, Rest2...> * v)
	{
		if constexpr (Index == 0) { return &v->element; }
		else { return GetImpl<Index - 1, Rest2...>(v); }
	}

	template<unsigned Index, unsigned First2, unsigned... Rest2>
	static constexpr unsigned GetElementSizeImpl()
	{
		if constexpr (Index == 0) { return First2; }
		else { return GetElementSizeImpl<Index - 1, Rest2...>(); }
	}

public:
	template<unsigned Index>
	auto Get() -> decltype(*GetImpl<Index, Values...>(this)) & { return *GetImpl<Index, Values...>(this); }

	template<unsigned Index>
	static constexpr unsigned GetElementSize() { return GetElementSizeImpl<Index, Values...>(); }
};
