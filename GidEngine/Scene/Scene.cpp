#include "Scene.hpp"

using namespace GidEngine;


Scene::Scene()
{
	ecs.updateNames = { Name("Update") };
}

void Scene::Update()
{
	ecs.Update();
}
