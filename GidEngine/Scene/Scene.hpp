#pragma once

#include "ECS/Manager.hpp"

namespace GidEngine
{
class Scene
{
public:
	Gid::ECS::Manager ecs;

public:
	Scene();

	void Update();

};
}
