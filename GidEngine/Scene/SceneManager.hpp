#pragma once

#include <vector>
#include <memory>

#include "Scene.hpp"

using namespace std;

class SceneManager
{
private:
	vector<shared_ptr<Scene>> scenes;

public:
	void Update() {}
};
