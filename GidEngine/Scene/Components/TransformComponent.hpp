#pragma once

#include "ECS/Component.hpp"
#include "Scene/Transform.hpp"

namespace GidEngine
{
struct TransformComponent : ECS::Component
{
	Transform<TransformComponent> transform;

	TransformComponent() : transform(this) {}
	~TransformComponent() override
	{
		for (auto & child : transform.GetChildren())
		{
			child->SetParent(transform.GetParent());
		}
		transform.SetParent(nullptr);
	}
};
}
