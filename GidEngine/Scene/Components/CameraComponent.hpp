#pragma once

#include "ECS/Component.hpp"
#include "Graphics/Camera.hpp"

namespace GidEngine
{
struct CameraComponent : ECS::Component
{
	Camera camera;
};
}
