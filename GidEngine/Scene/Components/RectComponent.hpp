#pragma once

#include "ECS/Component.hpp"
#include "Scene/Rect.hpp"

namespace GidEngine
{
struct RectComponent : ECS::Component
{
	Rectf rect;
};
}
