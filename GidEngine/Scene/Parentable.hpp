#pragma once

#include <vector>

using namespace std;

template<typename T>
class Parentable
{
private:
	T * parent = nullptr;
	vector<T *> children;

public:
	Parentable() = default;
	virtual ~Parentable() { SetParent(nullptr); }
	Parentable(Parentable const &) = delete;
	Parentable & operator=(Parentable const &) = delete;

	T * GetParent() { return parent; }
	T const * GetParent() const { return parent; }
	void SetParent(T * newParent)
	{
		auto oldParent = parent;

		parent = newParent;
		if (oldParent) { oldParent->RemoveChild(static_cast<T *>(this)); }
		if (newParent) { newParent->AddChild(static_cast<T *>(this)); }

		OnParentChanged();
		if (oldParent) { oldParent->OnChildrenChanged(); }
		if (newParent) { newParent->OnChildrenChanged(); }
	}

	vector<T *> const & GetChildren() const { return children; }

protected:
	virtual void OnParentChanged() {}
	virtual void OnChildrenChanged() {}

private:
	void AddChild(T * t) { children.push_back(t); }
	void RemoveChild(T * t) { children.erase(remove(children.begin(), children.end(), t), children.end()); }
};
