#include "Manager.hpp"

#include <map>

#include "System.hpp"
#include "Entity.hpp"

using namespace Gid;
using namespace ECS;

void Manager::Update(vector<Name> const & names)
{
	vector<Updateable> toUpdate;

	for (auto & entity : entities)
	{
		if (!entity->enabled) { return; }

		SystemDataMap systemsToAdd;
		for (auto const & system : systems)
		{
			DataPtr data = system->TryGetData(entity.get());
			if (data) { systemsToAdd[system] = data; }
		}
		if (systemsToAdd.empty()) { return; }

		if (sortFunc)
		{
			Updateable updateable{ entity, systemsToAdd };
			vector<Updateable>::iterator insertPos;
			auto comp = [&](Updateable const & a, Updateable const & b) { return sortFunc(a.entity, b.entity); };
			insertPos = upper_bound(toUpdate.begin(), toUpdate.end(), updateable, comp);
			toUpdate.emplace(insertPos, updateable);
		}
		else
		{
			toUpdate.push_back({ entity, systemsToAdd });
		}
	}

	for (auto const & name : names)
	{
		for (auto const & updateable : toUpdate)
		{
			for (auto & system : updateable.systemDataMap)
			{
				auto cb = system.first->GetCallback(name);
				if (cb)
				{
					cb(updateable.entity.get(), system.second.get());
				}
			}
		}
	}
}
