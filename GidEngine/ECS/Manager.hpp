#pragma once

#include <vector>
#include <map>
#include <memory>
#include <functional>

#include "Utils/Name.hpp"
#include "Utils/Algo.hpp"

#include "Entity.hpp"
#include "System.hpp"

namespace Gid
{
namespace ECS
{
struct Component;
struct Data;

class Manager
{
public:
	using EntityPtr = shared_ptr<Entity>;
	using SystemPtr = shared_ptr<SystemBase>;
	using DataPtr = shared_ptr<Data>;
	using SystemDataMap = map<SystemPtr, DataPtr, PtrValComp<SystemPtr>>;

private:
	struct Updateable
	{
		EntityPtr entity;
		SystemDataMap systemDataMap;
	};

public:
	vector<Name> updateNames;
	function<bool(EntityPtr const &, EntityPtr const &)> sortFunc = nullptr;

private:
	vector<EntityPtr> entities;
	vector<SystemPtr> systems;

public:
	vector<EntityPtr> const & GetEntities() const { return entities; };
	vector<SystemPtr> const & GetSystems() const { return systems; }

	auto AddEntity() { return entities.emplace_back(make_shared<Entity>()); }
	auto AddEntity(EntityPtr const & entity) { return entities.emplace_back(entity); }
	void RemoveEntity(Entity const * entity) { EraseIf(entities, SharedPtrPtrComp(entity)); }

	template<typename T, typename... TArgs>
	auto AddSystem(TArgs const & ... args) { return systems.push_back(make_shared<T>(args...)); }
	auto AddSystem(const SystemPtr & system) { return systems.push_back(system); }
	void RemoveSystem(SystemBase const * system) { EraseIf(systems, SharedPtrPtrComp(system)); }

	void Update() { Update(updateNames); }

private:
	void Update(vector<Name> const & names);
};
}
}
