#pragma once

#include <vector>
#include <map>
#include <memory>

#include "Entity.hpp"
#include "Data.hpp"
#include "Utils/Callback.hpp"
#include "Utils/Name.hpp"

using namespace std;

namespace Gid
{
namespace ECS
{
class SystemBase
{
protected:
	using CBFunc = function<void(Entity *, Data *)>;

public:
	int const sortOrder;

protected:
	map<Name, CBFunc> callbacks;

public:
	explicit SystemBase(int sortOrder) : sortOrder(sortOrder) {}
	virtual ~SystemBase() = default;
	bool operator<(SystemBase const & other) const { return sortOrder < other.sortOrder; }

	CBFunc const & GetCallback(Name const & name) { return callbacks[name]; }
	virtual shared_ptr<Data> TryGetData(Entity * entity) = 0;
};

template<typename... TComponents>
class System : public SystemBase
{
protected:
	using Components = tuple<shared_ptr<TComponents>...>;
	using CBFunc = function<void(Entity *, Components &)>;

private:
	struct ToupleData : public Data
	{
		Components components;
		explicit ToupleData(Components const & components) : components(components) {}
	};

public:
	explicit System(int sortOrder) : SystemBase(sortOrder) {}

	shared_ptr<Data> TryGetData(Entity * entity) override
	{
		auto c = entity->components;
		bool valid = true;
		auto components = make_tuple<shared_ptr<TComponents>...>(TryGetComponent<TComponents>(c, valid) ...);
		auto ret = make_shared<ToupleData>(components);
		return valid ? ret : nullptr;
	}

protected:
	void AddCallback(Name const & name, CBFunc const & cb)
	{
		callbacks[name] = [cb](Entity * e, Data * d)
		{
			cb(e, dynamic_cast<ToupleData *>(d)->components);
		};
	}

private:
	template<typename T>
	shared_ptr<T> TryGetComponent(vector<shared_ptr<Component>> & components, bool & valid)
	{
		auto it = find_if(components.begin(), components.end(), [](auto const & e) { return dynamic_cast<T *>(e.get()); });
		if (it == components.end())
		{
			valid = false;
			return nullptr;
		}
		auto ret = dynamic_pointer_cast<T>(*it);
		components.erase(it);
		return ret;
	}
};
}
}
