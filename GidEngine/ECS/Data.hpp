#pragma once

namespace Gid
{
namespace ECS
{
struct Data
{
	virtual ~Data() = default;
};
}
}
