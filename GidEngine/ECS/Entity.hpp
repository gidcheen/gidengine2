#pragma once

#include <vector>
#include <set>
#include <list>
#include <memory>

#include "Utils/Algo.hpp"
#include "Utils/UId.hpp"
#include "Manager.hpp"

using namespace std;

namespace Gid
{
namespace ECS
{
struct Component;

class Entity
{
private:
	using ComponentPtr = shared_ptr<Component>;

public:
	UId const uId = UId::Next();
	bool enabled = true;
	vector<ComponentPtr> components;

public:
	Entity() = default;
	explicit Entity(UId const & uId) : uId(uId) {}

	void AddComponent(ComponentPtr & component) { components.push_back(component); }
	template<typename T, typename... TArgs>
	shared_ptr<T> AddComponent(TArgs const & ... args) { return dynamic_pointer_cast<T>(components.emplace_back(make_shared<T>(args...))); }

	void RemoveComponent(ComponentPtr const & component) { Erase(components, component); }
	template<typename T>
	void RemoveComponent() { RemoveComponent(GetComponent<T>()); }

	template<typename T>
	shared_ptr<T> GetComponent() const
	{
		shared_ptr<T> ret;
		for (auto & c : components)
		{
			ret = dynamic_pointer_cast<T>(c);
			if (ret) { break; }
		}
		return ret;
	}

	template<typename T>
	vector<shared_ptr<T>> GetComponents() const
	{
		vector<ComponentPtr> ret;
		for (auto & component : components)
		{
			auto c = dynamic_pointer_cast<T>(component);
			if (c) { ret.push_back(c); }
		}
		return ret;
	}

	template<typename T>
	bool HasComponent() const { return GetComponent<T>() != nullptr; }
	template<typename T>
	bool HasComponents() const { return HasComponent<T>(); }
	template<typename Head, typename... Tail>
	bool HasComponents() const { return HasComponent<Head>() && HasComponents<Tail...>(); }
};
}
}