#pragma once

#include "Utils/UId.hpp"

namespace Gid
{
namespace ECS
{
struct Component
{
	UId const uId = UId::Next();
	Component() = default;
	explicit Component(UId const & uId) : uId(uId) {}
	virtual ~Component() = default;
};
}
}
