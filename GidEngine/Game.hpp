#pragma once

#include "Window.hpp"
#include "Graphics/Renderer/Renderer.hpp"
#include "Scene/Scene.hpp"

using namespace GidEngine;
using namespace Gid::Graphics;

namespace Gid
{
class Game
{
protected:
	Renderer renderer;
	Scene scene;

	Window window;
	CbHandle<Window *> updateHandle;
	vector<CbHandle<Window *>> cbHandles;

private:
	array<float, 3> lastDts;
	float lastTime = 0;
	float dt = 0;

public:
	Game();
	virtual ~Game() = default;

	float GetDt() const { return dt; }

protected:
	virtual void Update() {};

private:
	void OnUpdate();

	void CalculateDt();
};
}
