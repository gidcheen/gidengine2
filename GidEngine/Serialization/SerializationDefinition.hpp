#pragma once

#include <functional>
#include <type_traits>

#include "Utils/TypeInfo.hpp"
#include "Utils/Name.hpp"

using namespace std;

template<typename TSerializer, typename TSerializable>
class SerializationDefinition
{
};

template<typename TSerializer, typename TSerializable>
struct FieldSerializer
{
	using Serializer = TSerializer;
	using Data = typename Serializer::Data;
	using Serializable = TSerializable;

	Name const name;
	function<void(Serializer const &, Data &, Serializable const &)> const serialize;
	function<void(Serializer const &, Data const &, Serializable &)> const deserialize;

	template<typename TFieldType>
	FieldSerializer(Name const & name, TFieldType TSerializable::* field) :
		name(name),
		serialize(
			[field](Serializer const & serializer, Data & data, Serializable const & serializable)
			{
				serializer.Serialize(data, (serializable.*field));
			}),
		deserialize(
			[field](Serializer const & serializer, Data const & data, Serializable & serializable)
			{
				serializer.Deserialize(data, serializable.*field);
			})
	{
	}

	template<typename TGet, typename TSet>
	FieldSerializer(
		Name const & name,
		TGet (TSerializable::* getter)() const,
		void (TSerializable::* setter)(TSet)) :
		FieldSerializer<Serializer, Serializable>(
			function < TGet(Serializable	const &)>([getter](	Serializable const & serializable	) -> TGet { return (serializable.*getter)(); }),
			function<void(Serializable &, TSet)> ([setter](	Serializable & serializable, TSet	set) { (serializable.*setter)(set); }),
			name)
	{
	}

	template<typename TGet, typename TSet>
	FieldSerializer(
		function<TGet(Serializable const &)> const & getter,
		function<void(Serializable &, TSet)> const & setter,
		Name const & name) :
		name(name),
		serialize(
			[getter](Serializer const & serializer, Data & data, Serializable const & serializable)
			{
				serializer.Serialize(data, getter(serializable));
			}),
		deserialize(
			[getter, setter](Serializer const & serializer, Data const & data, Serializable & serializable)
			{
				auto ret = getter(serializable);
				serializer.Deserialize(data, ret);
				setter(serializable, ret);
			})
	{
	}

	template<typename TIt>
	using shit = function<TIt const &()>;
	/*
		template<typename TIt>
		FieldSerializer(Name const & name, function<TIt const &()> const & getBegin, function<TIt const &()> const & getEnd, shit<TIt> inserter = shit<TIt>()) :
			name(name),
			serialize(
				[getBegin, getEnd](Serializer const & serializer, Data & data, Serializable const & serializable)
				{
					serlizer.Serialize(data, getBegin, getEnd);
				}),
			deserialize(
				[getBegin, getEnd, inserter](Serializer const & serializer, Data const & data, Serializable & serializable)
				{
					serlizer.Deserialize(data, getBegin, getEnd, inserter);
				})
		{
		}*/
};
