#pragma once

#include <type_traits>
#include <vector>
#include <map>
#include <functional>
#include <memory>

#include "JsonSerializer.hpp"

#include "Utils/Json.hpp"
#include "Utils/Name.hpp"
#include "Utils/Logging.hpp"
#include "Utils/TypeInfo.hpp"

using namespace std;
/*
class PolymorphicSerializer
{
private:
	using SerializeF = function<Json::Value(void const *)>;
	using DeserializeF = function<void *(Json::Value const &)>;

	struct Data
	{
		SerializeF serialize;
		DeserializeF deserialize;
	};

	static map<Name const, PolymorphicSerializer::Data> serializers;

public:
	template<typename T>
	static void Add()
	{
		AssertIsPolimorphic<T>();
		auto typeName = GetTypeName<T>();
		if (serializers.find(typeName) != serializers.end())
		{
			LOG_WARNING("Type added twice to PolymorphicSerializer");
			return;
		}
		auto serialize = [](void const * value) { return Serializer<T>::ToJson(*(T *)value); };
		auto deserialize = [](Json::Value const & json) -> void * { return new T(Serializer<T>::FromJson(json)); };

		serializers[typeName] = Data{ serialize, deserialize };
	}

	template<typename T>
	static Json::Value Serialize(T const * value)
	{
		AssertIsPolimorphic<T>();
		Json::Value ret;
		auto name = GetTypeName(*value);
		ret["type"] = name.GetString();
		ret["data"] = serializers[name].serialize(value);
		return ret;
	}

	template<typename T>
	static shared_ptr<T> Deserialize(Json::Value const & json)
	{
		AssertIsPolimorphic<T>();
		auto & serializer = serializers[Name(json["type"].asString())];
		return shared_ptr<T>((T *)serializer.deserialize(json["data"]));
	}

private:
	template <typename T>
	static constexpr void AssertIsPolimorphic()
	{
		static_assert(is_polymorphic_v<T>, "only polymorphic types allowed");
	}
};
*/