#pragma once

#include <cstdint>
#include <type_traits>

#include "Utils/Json.hpp"
#include "SerializationDefinition.hpp"

struct JsonSerializer
{
	using Data = JsonV;

	template<typename TIt>
	void Serialize(JsonV & json, TIt & beginIt, TIt const & endIt) const
	{
		for (; beginIt != endIt; beginIt++)
		{
			Serialize(json.append(JsonV()), *beginIt);
		}
	}

	template<typename TIt>
	void Deserialize(JsonV & json, TIt beginIt, TIt endIt, function<TIt const &()> inserter = function<TIt const &()>()) const
	{
		for (int i = 0; i < json.size(); i++, beginIt++)
		{
			if (beginIt == endIt)
			{
				if (inserter)
				{
					beginIt = inserter();
					endIt = beginIt;
					endIt++;
				}
				else
				{
					break;
				}
			}
			Deserialize(json[i], *beginIt);
		}
	}

	template<typename T>
	void Serialize(JsonV & json, T const & value) const
	{
		for (auto const & fieldSerializer : SerializationDefinition<JsonSerializer, T>::GetFieldSerializers())
		{
			fieldSerializer.serialize(*this, json[fieldSerializer.name.GetString()], value);
		}
	}

	template<typename T>
	void Deserialize(JsonV const & json, T & value) const
	{
		for (auto const & fieldSerializer : SerializationDefinition<JsonSerializer, T>::GetFieldSerializers())
		{
			fieldSerializer.deserialize(*this, json[fieldSerializer.name.GetString()], value);
		}
	}


	void Serialize(JsonV & json, bool const & value) const { json = value; }
	void Deserialize(JsonV const & json, bool & value) const { value = json.asBool(); }

	void Serialize(JsonV & json, float const & value) const { json = value; }
	void Deserialize(JsonV const & json, float & value) const { value = json.asFloat(); }

	void Serialize(JsonV & json, double const & value) const { json = value; }
	void Deserialize(JsonV const & json, double & value) const { value = json.asDouble(); }

	void Serialize(JsonV & json, int8_t const & value) const { json = value; }
	void Deserialize(JsonV const & json, int8_t & value) const { value = (int8_t)json.asInt(); }

	void Serialize(JsonV & json, uint8_t const & value) const { json = value; }
	void Deserialize(JsonV const & json, uint8_t & value) const { value = (uint8_t)json.asUInt(); }

	void Serialize(JsonV & json, int16_t const & value) const { json = value; }
	void Deserialize(JsonV const & json, int16_t & value) const { value = (int16_t)json.asInt(); }

	void Serialize(JsonV & json, uint16_t const & value) const { json = value; }
	void Deserialize(JsonV const & json, uint16_t & value) const { value = (uint16_t)json.asInt(); }

	void Serialize(JsonV & json, int32_t const & value) const { json = value; }
	void Deserialize(JsonV const & json, int32_t & value) const { value = json.asInt(); }

	void Serialize(JsonV & json, uint32_t const & value) const { json = value; }
	void Deserialize(JsonV const & json, uint32_t & value) const { value = json.asUInt(); }

	void Serialize(JsonV & json, int64_t const & value) const { json = ((JsonV::Int64)value); }
	void Deserialize(JsonV const & json, int64_t & value) const { value = json.asInt64(); }

	void Serialize(JsonV & json, uint64_t const & value) const { json = (JsonV::UInt64)value; }
	void Deserialize(JsonV const & json, uint64_t & value) const { value = json.asUInt64(); }
};
