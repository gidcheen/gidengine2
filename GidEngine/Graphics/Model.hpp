#pragma once

#include <vector>

#include "BoundingBox.hpp"
#include "Mesh.hpp"

using namespace std;

namespace Gid
{
namespace Graphics
{
class Model
{
private:
	vector<shared_ptr<Mesh>> meshes;
	BoundingBox bounds;

public:
	explicit Model(vector<shared_ptr<Mesh>> const & meshes) { SetMeshes(meshes); }

	auto const & GetMeshes() const { return meshes; }
	void SetMeshes(vector<shared_ptr<Mesh>> const & meshes);

	auto const & GetBounds() const { return bounds; }

private:
	void RecalculateBounds();
};
}
}