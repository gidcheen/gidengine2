#include "BoundingBox.hpp"

using namespace Gid;
using namespace Graphics;

BoundingBox & BoundingBox::Add(BoundingBox const & other)
{
	min = min.Min(other.min);
	max = max.Max(other.max);
	return *this;
}

bool BoundingBox::Inside(const Vec3f & point) const
{
	bool x = point.x >= min.x && point.x <= max.x;
	bool y = point.y >= min.y && point.y <= max.y;
	bool z = point.z >= min.z && point.z <= max.z;
	return x && y && z;
}

bool BoundingBox::Overlaps(const BoundingBox & a, const BoundingBox & b)
{
	bool x = a.min.x <= b.max.x && a.max.x >= b.min.x;
	bool z = a.min.y <= b.max.y && a.max.y >= b.min.y;
	bool y = a.min.z <= b.max.z && a.max.z >= b.min.z;
	return x && y && z;
}

BoundingBox & BoundingBox::Transform(const Mat4f & trans)
{
	auto c = GetCenter();
	auto r = GetExtend();
	array<Vec3f, 8> transformed;
	for (int i = 0; i < 8; i++)
	{
		Vec3f mult;
		mult.x = float((i & (1 << 2)) == 0 ? 1 : -1);
		mult.y = float((i & (1 << 1)) == 0 ? 1 : -1);
		mult.z = float((i & (1 << 0)) == 0 ? 1 : -1);

		auto t = trans * Vec4f(c + Vec3f::CompMult(r, mult), 1);
		transformed[i] = Vec3f(t);
	}
	min = transformed[0];
	max = transformed[0];
	for (int i = 1; i < 8; i++)
	{
		min = min.Min(transformed[i]);
		max = max.Max(transformed[i]);
	}
	return *this;
}
