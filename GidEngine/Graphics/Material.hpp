#pragma once

#include <memory>

#include "GL/GLProgramPipeline.hpp"
#include "GL/GLTexture.hpp"
#include "Gid/Math/Maths.hpp"
#include "Utils/Json.hpp"

using namespace std;
using namespace Gid::GL;

namespace Gid
{
namespace Graphics
{
class Material
{
public:
	enum class Transparency : int
	{
		opaque = 0, cutout, translucent
	};

public:
	shared_ptr<GLProgramPipeline> shader;
	Transparency transparency;

private:
	map<string, int> ints;
	map<string, bool> bools;
	map<string, float> floats;
	map<string, Vec2f> vec2s;
	map<string, Vec3f> vec3s;
	map<string, Vec4f> vec4s;
	map<string, Mat4f> mat4s;
	map<string, shared_ptr<GLTexture>> textures;

public:
	explicit Material(shared_ptr<GLProgramPipeline> const & shader, Transparency transparency = Transparency::opaque);
	~Material() = default;

	void Set(string const & name, int value) { ints[name] = value; }
	void Set(string const & name, bool value) { bools[name] = value; }
	void Set(string const & name, float value) { floats[name] = value; }
	void Set(string const & name, Vec2f & value) { vec2s[name] = value; }
	void Set(string const & name, Vec3f & value) { vec3s[name] = value; }
	void Set(string const & name, Vec4f & value) { vec4s[name] = value; }
	void Set(string const & name, Mat4f & value) { mat4s[name] = value; }
	void Set(string const & name, shared_ptr<GLTexture> value) { textures[name] = value; }

	void Bind() const;
};
}
}
