#include "Material.hpp"

using namespace Gid;
using namespace Graphics;

Material::Material(shared_ptr<GLProgramPipeline> const & shader, Transparency transparency) :
	shader(shader),
	transparency(transparency)
{
}

void Material::Bind() const
{
	string m = "material.";
	shader->SetUniform(m + "transparency", static_cast<int>(transparency));

	for (auto & p : ints)
	{
		shader->SetUniform(m + p.first, p.second);
	}
	for (auto & p : bools)
	{
		shader->SetUniform(m + p.first, p.second);
	}
	for (auto & p : floats)
	{
		shader->SetUniform(m + p.first, p.second);
	}
	for (auto & p : vec2s)
	{
		shader->SetUniform(m + p.first, p.second);
	}
	for (auto & p : vec3s)
	{
		shader->SetUniform(m + p.first, p.second);
	}
	for (auto & p : vec4s)
	{
		shader->SetUniform(m + p.first, p.second);
	}
	for (auto & p : mat4s)
	{
		shader->SetUniform(m + p.first, p.second);
	}

	int i = 0;
	for (auto & p : textures)
	{
		if (!p.second) { continue; }
		shader->SetUniform(m + p.first, i);
		glActiveTexture(GL_TEXTURE0 + (GLenum)i);
		p.second->Bind();
		i++;
	}
}
