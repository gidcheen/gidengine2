#pragma  once

#include <cassert>

#include "Utils/Hash.hpp"
#include "Utils/Name.hpp"
#include "Utils/TypeInfo.hpp"

namespace Gid
{
namespace Graphics
{
struct RenderStepBase
{
	virtual ~RenderStepBase() = default;

	virtual bool operator<(RenderStepBase const & other) const = 0;

	virtual void Enable() {};
	virtual void Disable() {};

	virtual Name GetTypeName() const = 0;
};

template<typename T>
struct RenderStep : public RenderStepBase
{
	bool operator<(RenderStepBase const & other) const override
	{
		auto name = GetTypeName();
		auto otherName = other.GetTypeName();

		if (name != otherName) { return name < otherName; }

		auto thisPtr = dynamic_cast<T const *>(this);
		auto otherPtr = dynamic_cast<T const *> (&other);
		assert(thisPtr);
		assert(otherPtr);
		return *thisPtr < *otherPtr;
	}
};
}
}