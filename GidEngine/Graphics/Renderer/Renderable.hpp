#pragma once

#include <map>
#include <memory>
#include <functional>

#include "RenderStep.hpp"

#include "Utils/Hash.hpp"
#include "Utils/Name.hpp"
#include "Utils/TypeInfo.hpp"

using namespace std;

namespace Gid
{
namespace Graphics
{
struct Renderable
{
	vector<shared_ptr<RenderStepBase>> steps;
	bool operator<(Renderable const & other) const;
};
}
}
