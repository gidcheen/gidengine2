#include "Renderer.hpp"
#include "RenderStep.hpp"

#include <algorithm>

using namespace Gid;
using namespace Graphics;

void Renderer::Submit(Renderable const & renderable)
{
	auto upper = upper_bound(begin(renderables), end(renderables), renderable);
	renderables.emplace(upper, renderable);
}

void Renderer::Render()
{
	vector<shared_ptr<RenderStepBase>> activeSteps;
	for (auto const & renderable : renderables)
	{
		Render(renderable, activeSteps);
	}
	renderables.clear();
}

void Renderer::Render(Renderable const & renderable, vector<shared_ptr<RenderStepBase>> & activeSteps)
{
	auto minSteps = min(renderable.steps.size(), activeSteps.size());
	auto maxSteps = max(renderable.steps.size(), activeSteps.size());

	size_t start = 0;
	for (size_t i = 0; i < minSteps; i++)
	{
		start = i;
		if (activeSteps.size() <= i) { break; }
		if (*activeSteps[i] < *renderable.steps[i]) { break; }
	}

	for (auto i = start; i < maxSteps; i++)
	{
		auto previousStep = i < activeSteps.size() ? activeSteps[i] : nullptr;
		auto currentStep = i < renderable.steps.size() ? renderable.steps[i] : nullptr;

		if (previousStep) { previousStep->Disable(); }
		if (currentStep) { currentStep->Enable(); }

		if (activeSteps.size() <= i) { activeSteps.emplace_back(); }
		activeSteps[i] = currentStep;
	}
}
