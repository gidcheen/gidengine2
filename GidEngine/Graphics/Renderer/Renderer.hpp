#pragma once

#include <tuple>
#include <set>
#include <functional>

#include "Renderable.hpp"

using namespace std;

namespace Gid
{
namespace Graphics
{
class Renderer
{
private:
	vector<Renderable> renderables;

public:
	void Submit(vector<shared_ptr<RenderStepBase>> const & steps) { Submit(Renderable{ steps }); }
	void Submit(Renderable const & renderable);

	void Render();

private:
	void Render(Renderable const & renderable, vector<shared_ptr<RenderStepBase>> & activeSteps);
};
}
}
