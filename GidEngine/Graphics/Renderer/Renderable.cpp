#include "Renderable.hpp"

#include <set>

#include "RenderStep.hpp"
#include "Utils/Algo.hpp"

using namespace Gid;
using namespace Graphics;

bool Renderable::operator<(Renderable const & other) const
{
	auto count = min(steps.size(), other.steps.size());
	for (size_t i = 0; i < count; ++i)
	{
		if (*other.steps[i] < *steps[i]) { return false; }
		if (*steps[i] < *other.steps[i]) { return true; }
	}
	return steps.size() < other.steps.size();
}
