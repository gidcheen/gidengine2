#pragma once

#include <vector>
#include <numeric>

#include "GL/GL.hpp"
#include "BoundingBox.hpp"
#include "Gid/Math/Maths.hpp"
#include "Utils/Logging.hpp"

using namespace std;

namespace Gid
{
namespace Graphics
{
class MeshData
{
public:
	struct LayoutInfo
	{
		GLuint const location = 0;
		GLint const elementCount = 0;
		LayoutInfo(GLuint const location, GLint const elementCount) :
			location(location),
			elementCount(elementCount) {}
	};

public:
	vector<LayoutInfo> const layout;
	GLsizei const stride;

	vector<Vec<GLuint, 3>> indices;
	BoundingBox bounds;

private:
	vector<float> vertices;

public:
	explicit MeshData(vector<LayoutInfo> const & layout) :
		layout(layout), stride(GetStride(layout)) {}

	auto const & GetVertices() const { return vertices; }

	void SetVertices(vector<float> const & vertices);

	void AddVertex(vector<float> const & vertex);
	void AddVertices(vector<float> const & vertices);
	void RemoveVertex(size_t index);
	void RemoveVertices(size_t start, size_t end);

private:
	unsigned GetStride(vector<LayoutInfo> const & layout) const;
};
}
}
