#pragma once

#include "Gid/Math/Maths.hpp"
#include "GL/GLFrameBuffer.hpp"

using namespace Gid::GL;

namespace Gid
{
namespace Graphics
{
class Camera
{
private:
	float depth = 0;

	float nearPlane = 1;
	float farPlane = 100;
	float aspect = 16.f / 9.f;

	float fov = 60;
	float height = 10;

	bool isOrthographic = false;

	bool isDirty = true;

	GLFrameBuffer * frameBuffer = nullptr;
	Mat4f projMat;

public:
	Camera() { CalculateProjMat(); }

	float GetDepth() const { return depth; }
	void SetDepth(float depth);

	float GetNear() const { return nearPlane; }
	void SetNear(float nearPlane);

	float GetFar() const { return farPlane; }
	void SetFar(float farPlane);

	float GetAspect() const { return aspect; }
	void SetAspect(float aspect);

	float GetHeight() const { return height; }
	void SetHieght(float height);

	bool IsOrthographic() const { return isOrthographic; }
	void SetOrthographic(bool isOrthoGraphic);

	GLFrameBuffer const * GetFrameBuffer() const { return frameBuffer; }
	void SetFrameBuffer(GLFrameBuffer * frameBuffer) { this->frameBuffer = frameBuffer; }

	const Mat4f & GetProjMat();

private:
	void CalculateProjMat();
};
}
}
