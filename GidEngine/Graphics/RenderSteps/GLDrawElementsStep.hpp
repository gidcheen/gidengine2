#pragma once

#include "GL/GL.hpp"
#include "Graphics/Renderer/RenderStep.hpp"

namespace Gid
{
namespace Graphics
{
struct GLDrawElementsStep : public RenderStep<GLDrawElementsStep>
{
	GLint const count;

	explicit GLDrawElementsStep(GLint const & count) : count(count) {}
	~GLDrawElementsStep() override = default;

	bool operator<(GLDrawElementsStep const & other) const { return false; }

	void Enable() override
	{
		glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, nullptr);
	}

	Name GetTypeName() const override
	{
		static Name typeName = TypeName<decltype(*this)>();
		return typeName;
	}
};
}
}
