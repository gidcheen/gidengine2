#pragma once

#include <memory>

#include "Graphics/Renderer/RenderStep.hpp"

namespace Gid
{
namespace Graphics
{
template<typename T>
struct GLObjectBindStep : public RenderStep<GLObjectBindStep<T>>
{
	shared_ptr<T const> const glObject;

	explicit GLObjectBindStep(shared_ptr<T> const & glObject) : glObject(glObject) {}
	~GLObjectBindStep() override = default;

	bool operator<(GLObjectBindStep<T> const & other) const { return glObject->id < other.glObject->id; }

	void Enable() override { glObject->Bind(); }
	void Disable() override { glObject->Unbind(); }

	Name GetTypeName() const override
	{
		static Name typeName = TypeName<decltype(*this)>();
		return typeName;
	}
};
}
}
