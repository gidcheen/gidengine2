#include "Model.hpp"

#include <numeric>

using namespace Gid;
using namespace Graphics;

void Model::SetMeshes(vector<shared_ptr<Mesh>> const & meshes)
{
	this->meshes = meshes;
	RecalculateBounds();
}

void Model::RecalculateBounds()
{
	if (meshes.empty())
	{
		bounds = BoundingBox();
		return;
	}

	auto op = [](BoundingBox & b, shared_ptr<Mesh> const & m)
	{
		return b.Add(m->GetBounds());;
	};
	bounds = accumulate(begin(meshes) + 1, end(meshes), meshes.front()->GetBounds(), op);
}
