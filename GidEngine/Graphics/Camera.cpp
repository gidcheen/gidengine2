#include "Camera.hpp"

using namespace Gid;
using namespace Graphics;
using namespace Maths;

void Camera::SetDepth(float depth)
{
	this->depth = depth;
	isDirty = true;
}

void Camera::SetNear(float nearPlane)
{
	this->nearPlane = nearPlane;
	isDirty = true;
}

void Camera::SetFar(float farPlane)
{
	this->farPlane = farPlane;
	isDirty = true;
}

void Camera::SetAspect(float aspect)
{
	this->aspect = aspect;
	isDirty = true;
}

void Camera::SetHieght(float height)
{
	this->height = height;
	isDirty = true;
}

void Camera::SetOrthographic(bool isOrthoGraphic)
{
	this->isOrthographic = isOrthoGraphic;
	isDirty = true;
}

Mat4f const & Camera::GetProjMat()
{
	if (isDirty) { CalculateProjMat(); };
	return projMat;
}

void Camera::CalculateProjMat()
{
	if (isOrthographic)
	{
		float halfWidth = height * aspect * 0.5f;
		float halfHeight = height * 0.5f;
		projMat = Math::Orthographic(-halfWidth, halfWidth, -halfHeight, halfHeight, nearPlane, farPlane);
	}
	else
	{
		projMat = Math::Perspective(fov, aspect, nearPlane, farPlane);
	}
}
