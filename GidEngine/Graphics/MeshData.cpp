#include "MeshData.hpp"

using namespace Gid;
using namespace Graphics;

void MeshData::SetVertices(vector<float> const & vertices)
{
	this->vertices.clear();
	AddVertices(vertices);
}

void MeshData::AddVertex(vector<float> const & vertex)
{
	if (stride != vertex.size())
	{
		LOG_WARNING(
			"Trying to add vertex with stride: " +
			to_string(vertex.size()) +
			"to surface with stride of: " +
			to_string(stride));
		return;
	}

	vertices.insert(end(vertices), begin(vertex), end(vertex));
}

void MeshData::AddVertices(vector<float> const & vertices)
{
	if (vertices.size() % stride != 0)
	{
		LOG_WARNING(
			"Trying to add vertices that dont fit layout. " +
			to_string(vertices.size() / stride) +
			" vertices with " +
			to_string(vertices.size() % stride) +
			" left over");
		return;
	}
	this->vertices.insert(end(this->vertices), begin(vertices), end(vertices));
}

void MeshData::RemoveVertex(size_t index)
{
	RemoveVertices(index, index + 1);
}

void MeshData::RemoveVertices(size_t start, size_t end)
{
	auto startPos = begin(vertices) + start * stride;
	auto endPos = begin(vertices) + end * stride;
	vertices.erase(startPos, endPos);
}

unsigned MeshData::GetStride(vector<MeshData::LayoutInfo> const & layout) const
{
	auto op = [](auto prev, auto const & v)
	{
		return prev + v.elementCount;
	};
	return accumulate(begin(layout), end(layout), 0u, op);
}
