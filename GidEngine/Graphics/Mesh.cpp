#include "Mesh.hpp"

using namespace Gid;
using namespace Graphics;

void Mesh::Set(MeshData const & meshData)
{
	indexBuffer->Set(meshData.indices.front().elements.data(), meshData.indices.size() * meshData.indices.front().dimX);
	vertexBuffer->Set(meshData.GetVertices());

	for (auto layoutInfo : layoutInfos)
	{
		vertexArray->DisableVertexAttrib(layoutInfo.location);
	}

	GLsizei offset = 0;
	for (auto const & layout : meshData.layout)
	{
		vertexArray->EnableVertexAttrib(vertexBuffer, layout.location, layout.elementCount, meshData.stride, offset);
		offset += layout.elementCount;
	}

	bounds = meshData.bounds;
}
