#pragma once

#include <memory>
#include <vector>

#include "MeshData.hpp"
#include "GL/GLBuffer.hpp"
#include "GL/GLVertexArray.hpp"
#include "BoundingBox.hpp"

using namespace std;

namespace Gid
{
using namespace GL;
namespace Graphics
{
class Mesh
{
private:
	shared_ptr<GLIndexBuffer> indexBuffer = make_shared<GLIndexBuffer>();
	shared_ptr<GLVertexBuffer> vertexBuffer = make_shared<GLVertexBuffer>();
	shared_ptr<GLVertexArray> vertexArray = make_shared<GLVertexArray>();
	BoundingBox bounds;
	vector<MeshData::LayoutInfo> layoutInfos;

public:
	explicit Mesh(MeshData const & meshData) { Set(meshData); }

	auto const & GetIndexBuffer() const { return indexBuffer; }
	auto const & GetVertexBuffer() const { return vertexBuffer; }
	auto const & GetVertexArray() const { return vertexArray; }
	auto const & GetBounds() const { return bounds; }
	auto const & GetLayoutInfos() const { return layoutInfos; }

	void Set(MeshData const & meshData);
};
}
}
