#pragma once

#include "Gid/Math/Maths.hpp"

#undef min
#undef max

using namespace std;
using namespace Gid::Maths;

namespace Gid
{
namespace Graphics
{
class BoundingBox
{
public:
	Vec3f min;
	Vec3f max;

public:
	BoundingBox() = default;
	BoundingBox(const Vec3f & min, const Vec3f & max) : min(min), max(max) {}

	Vec3f GetSize() const { return max - min; }
	Vec3f GetExtend() const { return GetSize() / 2; }
	Vec3f GetCenter() const { return min + GetExtend(); }

	float GetRadius() const { return GetExtend().Lenght(); }
	float GetRadiusSquared() const { return GetExtend().LenghtSquared(); }

	BoundingBox & Add(BoundingBox const & other);

	bool Inside(const Vec3f & point) const;

	bool Overlaps(const BoundingBox & other) const { return Overlaps(*this, other); }
	static bool Overlaps(const BoundingBox & a, const BoundingBox & b);

	BoundingBox Transformed(const Mat4f & trans) const { return BoundingBox(*this).Transform(trans); }
	BoundingBox & Transform(const Mat4f & trans);
};
}
}
