#include "Game.hpp"

#include <numeric>

#include <GLFW/glfw3.h>
#include "Gid/Math/Maths.hpp"

#include <reactphysics3d/reactphysics3d.h>

using namespace Gid;

Game::Game() :
	window("GidEngine Game", { 400, 400 }),
	updateHandle(window.AddCallback(Window::Callbacks::Update, [&](auto w) { OnUpdate(); }))
{
	lastDts.fill(1 / 60.f);
	cbHandles.push_back(
		window.AddCallback(
			Window::Callbacks::Size, [&](auto w)
			{
				const auto size = window.GetSize();
				glViewport(0, 0, size.x, size.y);
			}));
	window.Open();
}

void Game::OnUpdate()
{
	CalculateDt();
	Update();
	scene.Update();
	renderer.Render();
}

void Game::CalculateDt()
{
	const auto currentTime = static_cast<float>(glfwGetTime());
	for (auto i = lastDts.size() - 1; i >= 1; i--) { lastDts[i] = lastDts[i - 1]; }
	lastDts[0] = currentTime - lastTime;
	dt = accumulate(lastDts.begin(), lastDts.end(), 0.f);
	lastTime = currentTime;
}
